using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using EvansWebAPI.Common;
using EvansWebAPI.Contexts;
using EvansWebAPI.Entities;
using EvansWebAPI.EntityLanguages;
using EvansWebAPI.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace EvansWebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            //Automapper Inicialization
            services.AddAutoMapper(configAction =>
            {
                configAction.CreateMap<ManufacturerAddDTO, Manufacturer>().ReverseMap();
                configAction.CreateMap<ManufacturerDTO, Manufacturer>().ReverseMap();
                configAction.CreateMap<DefaultLanguageValues, DefaultLanguageValueDTO>().ReverseMap();
                configAction.CreateMap<CategoryAddDTO, Category>().ReverseMap();
                configAction.CreateMap<CategoryDTO, Category>().ReverseMap();
                configAction.CreateMap<CollectionDTO, Category>().ReverseMap();
                configAction.CreateMap<CollectionAddDTO, Category>().ReverseMap();
                configAction.CreateMap<PromotionAddDTO, Category>().ReverseMap();
                configAction.CreateMap<ProductAddDTO, Product>().ReverseMap();
                configAction.CreateMap<ProductCategoryMappingAddDTO, ProductCategoryMapping>().ReverseMap();
                configAction.CreateMap<CollectionCategoryMappingAddDTO, ProductCategoryMapping>().ReverseMap();
                configAction.CreateMap<ProductInfoDTO, Product>().ReverseMap();
                configAction.CreateMap<ProductShortInfoDTO, Product>().ReverseMap();
                configAction.CreateMap<ProductDescriptionValueAddDTO, ProductDescriptionValue>().ReverseMap();
                configAction.CreateMap<ProductDocumentAddDTO, ProductDocument>().ReverseMap();
                configAction.CreateMap<ProductLanguageValuesDTO, DefaultLanguageValues>().ReverseMap();
                configAction.CreateMap<RelatedProductDTO, RelatedProduct>().ReverseMap();
                configAction.CreateMap<CategoryLanguageValuesDTO, DefaultLanguageValues>().ReverseMap();
                configAction.CreateMap<CollectionUpdateDTO, Category>().ReverseMap();
                configAction.CreateMap<PromotionDTO, Category>().ReverseMap();
                configAction.CreateMap<PromotionCategoryMappingAddDTO, ProductCategoryMapping>().ReverseMap();
                configAction.CreateMap<SpecificationAttributeOptionInfoDTO, SpecificationAttributeOption>().ReverseMap();
                configAction.CreateMap<SpecificationAttributeOptionAddDTO, SpecificationAttributeOption>().ReverseMap();
                configAction.CreateMap<ProductSpecificationAttributesAddDTO, ProductSpecificationAttributeMapping>().ReverseMap();
                configAction.CreateMap<ProductDescriptionTypeInfoDTO, ProductDescriptionType>().ReverseMap();
                configAction.CreateMap<DiscountAppliedToProductsAddDTO, DiscountAppliedToProducts>().ReverseMap();
                configAction.CreateMap<DiscountAppliedToCategoriesAddDTO, DiscountAppliedToCategories>().ReverseMap();
                configAction.CreateMap<DiscountInfoDTO, Discount>().ReverseMap();

            }, typeof(Startup));

            //Common Services
            services.AddScoped<CommonMethods>();
            services.AddScoped<CommonCategory>();

            //Context
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
                 options.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateIssuer = false,
                     ValidateAudience = false,
                     ValidateLifetime = true,
                     ValidateIssuerSigningKey = true,
                     IssuerSigningKey = new SymmetricSecurityKey(
                     Encoding.UTF8.GetBytes(Configuration["jwt:key"])),
                     ClockSkew = TimeSpan.Zero
                 });

            //Swager
            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1",
                    new Microsoft.OpenApi.Models.OpenApiInfo
                    {
                        Version = "V1",
                        Title = "Evans - Products Web API",
                        Description = "Web API para consultar y enviar información relacionadas con los productos del e-commerce.",
                        Contact = new Microsoft.OpenApi.Models.OpenApiContact()
                        {
                            Name = "Mario Rodas/Carlos López",
                            Email = "marioalberto_753@hotmail.com / carloslop@gmail.com",
                            Url = new Uri("https://www.linkedin.com/in/mariorodasb/")
                        }
                    });

                config.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                                    Enter 'Bearer' [space] and then your token in the text input below.
                                    \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = Microsoft.OpenApi.Models.ParameterLocation.Header,
                    Type = Microsoft.OpenApi.Models.SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                config.AddSecurityRequirement(new Microsoft.OpenApi.Models.OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });

                //var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                //var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //config.IncludeXmlComments(xmlPath);
                //config.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
            });

            //NewtonSoft Json Setup
            services.AddControllers()
                .AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint("v1/swagger.json", "Moba - Contpaq Web API");
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(config =>
                {
                    config.SwaggerEndpoint("v1/swagger.json", "Moba - Contpaq Web API");
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
