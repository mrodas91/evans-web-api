﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EvansWebAPI.Common;
using EvansWebAPI.Contexts;
using EvansWebAPI.Entities;
using EvansWebAPI.EntityLanguages;
using EvansWebAPI.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EvansWebAPI.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CollectionController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;
        private readonly CommonCategory commonCategory;
        private readonly CommonMethods commonMethods;

        public CollectionController(ApplicationDbContext context, IMapper mapper, IConfiguration configuration,
            CommonMethods commonMethods, CommonCategory commonCategory)
        {
            this.context = context;
            this.mapper = mapper;
            this.configuration = configuration;
            this.commonMethods = commonMethods;
            this.commonCategory = commonCategory;
        }

        /// <summary>
        /// Retorna todas las coleeciones
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAll")]
        public async Task<ActionResult<List<CollectionDTO>>> GetAll()
        {
            List<Category> collectionList = context.Category.
            Where(x => x.ParentCategoryId == Convert.ToInt32(configuration["DefaultDbValues:CollectionCategoryId"])).ToList();

            if (collectionList == null)
                return NotFound();

            List<CollectionDTO> collectionDTO = mapper.Map<List<CollectionDTO>>(collectionList);

            return collectionDTO;
        }

        /// <summary>
        /// Retorna una coleción por su Id
        /// </summary>
        /// <param name="id">Id de Colección</param>
        /// <returns></returns>
        [HttpGet("GetCollectionById/{id}", Name = "GetCollectionById")]
        public async Task<ActionResult<CollectionDTO>> GetCollectionById(int id)
        {
            if (!commonCategory.IsCollection(id))
                return BadRequest("This CategoryId does not belong to a Collection.");

            Category collection = await context.Category.FirstOrDefaultAsync(x => x.Id == id);

            if (collection == null)
                return NotFound();

            CollectionDTO collectionDTO = mapper.Map<CollectionDTO>(collection);

            return collectionDTO;
        }

        /// <summary>
        /// Retorna una Colección por su Nombre
        /// </summary>
        /// <param name="name">Nombre de la Colección</param>
        /// <returns></returns>
        [HttpGet("GetCollectionByName/{name}")]
        public async Task<ActionResult<CollectionDTO>> GetCollectionByName(string name)
        {
            Category collection = await context.Category.
                FirstOrDefaultAsync(x => x.Name.Contains(name) && x.ParentCategoryId == Convert.ToInt32(configuration["DefaultDbValues:CollectionCategoryId"]));

            if (collection == null)
                return NotFound();

            CollectionDTO collectionDTO = mapper.Map<CollectionDTO>(collection);

            return collectionDTO;
        }

        /// <summary>
        /// Retorna el arbol de Colecciones
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetArbolColecciones")]
        public async Task<ActionResult<List<CategoryTreeDTO>>> GetArbolColecciones()
        {
            return await commonCategory.GetAllByName("Colecciones");
        }

        /// <summary>
        /// Crea una colecion nueva.
        /// Las colecciones son categorias dentro de la base de datos, estas se asignan directamente a la MegaFamilia "Coleciones".
        /// </summary>
        /// <param name="collectionDTO">Modelo</param>
        /// <returns></returns>
        [HttpPost("CreateCollection")]
        public async Task<ActionResult> CreateCollection([FromBody] CollectionAddDTO collectionDTO)
        {
            Category collection = mapper.Map<Category>(collectionDTO);

            collection.ParentCategoryId = Convert.ToInt32(configuration["DefaultDbValues:CollectionCategoryId"]);
            collection.IsCollection = true;
            
            if (collection.PageSize.Equals(0))
                collection.PageSize = Convert.ToInt32(configuration["DefaultDbValues:PageSize"]);
            if (string.IsNullOrWhiteSpace(collection.PageSizeOptions))
                collection.PageSizeOptions = configuration["DefaultDbValues:PageSizeOptions"];

            collection.AllowCustomersToSelectPageSize = true;
            collection.CategoryTemplateId = 1;
            collection.CreatedOnUtc = DateTime.UtcNow;
            collection.UpdatedOnUtc = collection.CreatedOnUtc;
            

            context.Category.Add(collection);
            await context.SaveChangesAsync();

            if (collection.Id != 0)
            {
                #region URL Settings
                bool result = await commonMethods.CreateUrlRecord("Category", collection.Id, collection.Name, Convert.ToInt32(configuration["DefaultDbValues:SpanishLanguageId"]));
                #endregion

                #region Language Settings
                if (collection.CategoryLanguageValues is not null)
                    await commonMethods.AddEnglishValues(collection.CategoryLanguageValues, "Category", collection.Id);
                #endregion

                #region Adding Picture
                if (collectionDTO.PicturePath is not null)
                    await commonCategory.AddCategoryPicture(collectionDTO.PicturePath, collection);
                #endregion
            }

            CollectionDTO collectionCreatedDTO = mapper.Map<CollectionDTO>(collection);

            return new CreatedAtRouteResult("GetCollectionById", new { id = collection.Id }, collectionCreatedDTO);
        }

        /// <summary>
        /// Actualiza los datos de una colección
        /// Las colecciones son categorias dentro de la base de datos, estas se asignan directamente a la MegaFamilia "Coleciones".
        /// </summary>
        /// <param name="collectionDTO">Modelo</param>
        /// <returns></returns>
        [HttpPut("UpdateCollection")]
        public async Task<ActionResult> UpdateCollection([FromBody] CollectionUpdateDTO collectionDTO)
        {
            Category collection = await context.Category.FirstOrDefaultAsync(x => x.Id == collectionDTO.CollectionId);

            if (collection is null)
                return NotFound();

            if (!commonCategory.IsCollection(collection.Id))
                return BadRequest("The Id does not belong to a Collection.");

            if (collectionDTO.Name is not null) collection.Name = collectionDTO.Name;
            if (collectionDTO.Description is not null) collection.Description = collectionDTO.Description;
            if (collectionDTO.PageSize is not null) collection.PageSize = (int)collectionDTO.PageSize;
            if (collectionDTO.PageSizeOptions is not null) collection.PageSizeOptions = collectionDTO.PageSizeOptions;
            if (collectionDTO.DisplayOrder is not null) collection.DisplayOrder = (int)collectionDTO.DisplayOrder;

            collection.UpdatedOnUtc = DateTime.UtcNow;

            await context.SaveChangesAsync();

            #region UrlRecord
            if (collectionDTO.Name is not null)
            {
                await commonMethods.UpdateUrlRecord("Category", collection.Id, collection.Name,
                        Convert.ToInt32(configuration["DefaultDbValues:SpanishLanguageId"]));
            }
            #endregion

            #region Language Settings
            if (collectionDTO.CategoryLanguageValues is not null)
            {
                DefaultLanguageValues languageValues = mapper.Map<DefaultLanguageValues>(collectionDTO.CategoryLanguageValues);
                await commonMethods.AddEnglishValues(languageValues, "Category", collection.Id);
            }
            #endregion

            return Ok();
        }

        /// <summary>
        /// Agrega los valores en inglés para Colecciones
        /// </summary>
        /// <param name="languageValuesDTO"></param>
        /// <returns></returns>
        [HttpPut("AddCollectionLangageValues")]
        public async Task<ActionResult> AddCollectionLangageValues([FromBody] List<CategoryLanguageValuesDTO> languageValuesDTO)
        {
            if (languageValuesDTO.Where(x => x.EntityId == null).ToList().Count() > 0)
                return BadRequest("EntityId is required for all of the elements in the list.");
            if (languageValuesDTO.Where(x => commonCategory.CategoryExist((int)x.EntityId) == true).ToList().Count() < languageValuesDTO.Count())
                return BadRequest("Not all of the Categories in the model exist.");
            if (languageValuesDTO.Where(x => commonCategory.IsCollection((int)x.EntityId) == true).ToList().Count() < languageValuesDTO.Count())
                return BadRequest("Not all of the CategoryId belong to a Collection.");

            List<DefaultLanguageValues> languageValuesList = mapper.Map<List<DefaultLanguageValues>>(languageValuesDTO);
            await commonMethods.AddEnglishValues(languageValuesList, "Category");

            return Ok();
        }

        /// <summary>
        /// Habilita o deshabilita un coleción
        /// </summary>
        /// <param name="collectionId">Id de Coleción</param>
        /// <returns></returns>
        [HttpPut("EnableDisableCollection/{collectionId}")]
        public async Task<ActionResult> EnableDisableCollection(int collectionId)
        {
            Category collection = await context.Category.FirstOrDefaultAsync(x => x.Id == collectionId);

            if (collection is null)
                return NotFound();

            collection.Published = !collection.Published;
            collection.UpdatedOnUtc = DateTime.UtcNow;
            await context.SaveChangesAsync();

            await commonMethods.EnableDisableUrlRecords("Category", collectionId);

            string result = collection.Published == true ? "Enable" : "Disable";

            return Ok(result);
        }

        /// <summary>
        /// Elimina una Coleción
        /// </summary>
        /// <param name="collectionId">Colleción Id</param>
        /// <returns></returns>
        [HttpDelete("RemoveCollection/{collectionId}")]
        public async Task<ActionResult> RemoveCollection(int collectionId)
        {
            if (!commonCategory.CategoryExist(collectionId))
                return BadRequest("The CategoryId of this Collection does not exist.");
            if (!commonCategory.IsCollection(collectionId))
                return BadRequest("The CategoryId does not belong to a Collection.");

            List<ProductCategoryMapping> collectionProductList = await context.ProductCategoryMapping.Where(x => x.CategoryId == collectionId).ToListAsync();
            if (collectionProductList.Count > 0)
                return BadRequest("The Collection can't be remove becuase it has " + collectionProductList.Count + " Product(s) related.");

            Category collectionToRemove = await context.Category.FirstOrDefaultAsync(x => x.Id == collectionId);
            context.Category.Remove(collectionToRemove);
            await context.SaveChangesAsync();

            await commonMethods.DeleteUrlRecords("Category", collectionToRemove.Id);
            await commonMethods.DeleteEnglishValues("Category", collectionToRemove.Id);

            return Ok();
        }

        /// <summary>
        /// Agrega un producto a una Colección
        /// </summary>
        /// <param name="collectionDTO">Modelo</param>
        /// <returns></returns>
        [HttpPost("AddProductToCollection")]
        public async Task<ActionResult> AddProductToCollection([FromBody] CollectionCategoryMappingAddDTO collectionDTO)
        {
            if (!commonCategory.CategoryExist(collectionDTO.CategoryId))
                return BadRequest("The CategoryId of this Collection does not exist.");
            if (!commonCategory.IsCollection(collectionDTO.CategoryId))
                return BadRequest("The CategoryId does not belong to a Collection.");
            if (!commonMethods.ProductExits(collectionDTO.ProductId))
                return BadRequest("The ProductId does not exist.");
            if (context.ProductCategoryMapping.ToList().Where(x => x.CategoryId == collectionDTO.CategoryId && x.ProductId == collectionDTO.ProductId).ToList().Count() > 0)
                return BadRequest("The ProductId is already related to this Collection.");

            ProductCategoryMapping categoryMapping = mapper.Map<ProductCategoryMapping>(collectionDTO);

            await context.AddAsync(categoryMapping);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Agrega una serie de Productos a una Colección
        /// </summary>
        /// <param name="categoriesDTO">Lista del Modelo</param>
        /// <returns></returns>
        [HttpPost("AddProductToCollectionBulk")]
        public async Task<ActionResult> AddProductToCollectionBulk([FromBody] List<CollectionCategoryMappingAddDTO> categoriesDTO)
        {
            List<CollectionCategoryMappingAddDTO> x = categoriesDTO.Where(x => commonCategory.IsCollection(x.CategoryId) == false).ToList();

            if (categoriesDTO.Where(x => commonCategory.CategoryExist(x.CategoryId) == true).ToList().Count() < categoriesDTO.Count())
                return BadRequest("Not all of the CategoryId Collections exist.");
            if (categoriesDTO.Where(x => commonCategory.IsCollection(x.CategoryId) == true).ToList().Count() < categoriesDTO.Count())
                return BadRequest("Not all of the CategoryIds belong to a Collection.");
            if (categoriesDTO.Where(x => commonMethods.ProductExits(x.ProductId) == true).ToList().Count() < categoriesDTO.Count())
                return BadRequest("Not all of the ProductIds exist.");
            if (context.ProductCategoryMapping.ToList().Where(x => categoriesDTO.Any(y => x.CategoryId == y.CategoryId && x.ProductId == y.ProductId)).ToList().Count() > 0)
                return BadRequest("The ProductId is already related to this Collection.");

            List<ProductCategoryMapping> categoriesMapping = mapper.Map<List<ProductCategoryMapping>>(categoriesDTO);

            await context.AddRangeAsync(categoriesMapping);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Elimina un producto de una Coleción
        /// </summary>
        /// <param name="collectionDTO">Modelo</param>
        /// <returns></returns>
        [HttpDelete("RemoveProductFromCollection")]
        public async Task<ActionResult> RemoveProductFromCollection([FromBody] CollectionCategoryMappingRemoveDTO collectionDTO)
        {
            if (!commonCategory.CategoryExist(collectionDTO.CategoryId))
                return BadRequest("The CategoryId of this Collection does not exist.");
            if (!commonCategory.IsCollection(collectionDTO.CategoryId))
                return BadRequest("The CategoryId does not belong to a Collection.");
            if (!commonMethods.ProductExits(collectionDTO.ProductId))
                return BadRequest("The ProductId does not exist.");

            ProductCategoryMapping categoryMappingToRemove = await context.ProductCategoryMapping.
                FirstOrDefaultAsync(x => x.CategoryId == collectionDTO.CategoryId && x.ProductId == collectionDTO.ProductId);

            if (categoryMappingToRemove is null)
                return BadRequest("No values found.");

            context.Remove(categoryMappingToRemove);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Remueve una serie de productos de una o varias Colecciones
        /// </summary>
        /// <param name="categoriesDTO"></param>
        /// <returns></returns>
        [HttpDelete("RemoveProductFromCollectionBulk")]
        public async Task<ActionResult> RemoveProductFromCollectionBulk([FromBody] List<CollectionCategoryMappingAddDTO> categoriesDTO)
        {
            if (categoriesDTO.Where(x => commonCategory.CategoryExist(x.CategoryId) == true).ToList().Count() < categoriesDTO.Count())
                return BadRequest("Not all of the CategoryId Collections exist.");
            if (categoriesDTO.Where(x => commonCategory.IsCollection(x.CategoryId) == true).ToList().Count() < categoriesDTO.Count())
                return BadRequest("Not all of the CategoryIds belong to a Collection.");
            if (categoriesDTO.Where(x => commonMethods.ProductExits(x.ProductId) == true).ToList().Count() < categoriesDTO.Count())
                return BadRequest("Not all of the ProductIds exist.");

            List<ProductCategoryMapping> categoriesMappingToRemove = context.ProductCategoryMapping.ToList()
                    .Where(x => categoriesDTO.Any(y => x.CategoryId == y.CategoryId && x.ProductId == y.ProductId)).ToList();

            if(categoriesMappingToRemove.Count() == 0)
                return NotFound("No values were found.");

            if (categoriesMappingToRemove.Count < categoriesDTO.Count())
                return BadRequest("Not all of the relation exist in the db.");

            context.RemoveRange(categoriesMappingToRemove);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Remover todos los productos de una coleción
        /// </summary>
        /// <param name="collectionId">Id de Colleción</param>
        /// <returns></returns>
        [HttpDelete("RemoveAllProductsFromCollection/{collectionId}")]
        public async Task<ActionResult> RemoveAllProductsFromCollection(int collectionId)
        {
            if (!commonCategory.CategoryExist(collectionId))
                return BadRequest("The CategoryId of this Collection does not exist.");
            if (!commonCategory.IsCollection(collectionId))
                return BadRequest("The CategoryId does not belong to a Collection.");
            

            List<ProductCategoryMapping> collectionMappingToRemove = await context.ProductCategoryMapping.
                Where(x => x.CategoryId == collectionId).ToListAsync();

            if (collectionMappingToRemove.Count() == 0)
                return BadRequest("No values found.");

            context.RemoveRange(collectionMappingToRemove);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Agrega una imagen a la coleción
        /// </summary>
        /// <param name="collectionPictureAddDTO">Model</param>
        /// <returns></returns>
        [HttpPost("AddCategoryPicture")]
        public async Task<ActionResult> AddCollectionPicture([FromBody] CategoryPictureAddDTO collectionPictureAddDTO)
        {
            if (!commonCategory.CategoryExist(collectionPictureAddDTO.CategoryId))
                return BadRequest("The CategoryId of this Collection does not exist.");
            if (!commonCategory.IsCollection(collectionPictureAddDTO.CategoryId))
                return BadRequest("The CategoryId does not belong to a Collection.");

            Category category = await context.Category.FirstOrDefaultAsync(x => x.Id == collectionPictureAddDTO.CategoryId);

            if (category is null)
                return NotFound();

            await commonCategory.AddCategoryPicture(collectionPictureAddDTO.LocalPath, category);

            return Ok();
        }
    }
}
