﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EvansWebAPI.Common;
using EvansWebAPI.Contexts;
using EvansWebAPI.Entities;
using EvansWebAPI.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EvansWebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DiscountController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly CommonMethods commonMethods;
        private readonly CommonCategory commonCategory;

        public DiscountController(ApplicationDbContext context, IMapper mapper, CommonMethods commonMethods, CommonCategory commonCategory)
        {
            this.context = context;
            this.mapper = mapper;
            this.commonMethods = commonMethods;
            this.commonCategory = commonCategory;
        }

        /// <summary>
        /// Obtiene la informacion de un descuento por su Id
        /// </summary>
        /// <param name="discountId"></param>
        /// <returns></returns>
        [HttpGet("GetDiscountInfoById/{discountId}")]
        public async Task<ActionResult<DiscountInfoDTO>> GetDiscountInfo(int discountId)
        {
            Discount discount = await context.Discount.
                    Include(x => x.DiscountAppliedToProducts).
                    Include(x => x.DiscountAppliedToCategories).
                    FirstOrDefaultAsync(x => x.Id == discountId);

            DiscountInfoDTO discountDTO = mapper.Map<DiscountInfoDTO>(discount);

            switch(discount.DiscountTypeId)
            {
                case 1:
                    discountDTO.DiscountType = "Aplicado al Total de la Orden";
                    break;

                case 2:
                    discountDTO.DiscountType = "Aplicado a Productos";
                    break;

                case 5:
                    discountDTO.DiscountType = "Aplicado a Categorías";
                    break;

                case 10:
                    discountDTO.DiscountType = "Aplicado al Envío";
                    break;

                default:
                    discountDTO.DiscountType = "Tipo de descuento no válido.";
                    break;
            }

            switch (discount.DiscountLimitationId)
            {
                case 0:
                    discountDTO.DiscountLimitation = "Sin Límite";
                    break;

                case 15:
                    discountDTO.DiscountLimitation = "Limitado a N veces";
                    break;

                case 25:
                    discountDTO.DiscountLimitation = "N veces por cliente";
                    break;

                default:
                    discountDTO.DiscountLimitation = "Tipo de limitación no válida.";
                    break;

            }

            return discountDTO;
        }

        //GetDiscountInfoByName

        /// <summary>
        /// Crea un descuento
        /// DiscountTypeId use 1 = Total orden, 2 = Productos, 3 = Categorías, 4 = Envio
        /// DiscountLimitationId use 0 = Ilimitado, 1 = Limitado a N veces, 2 = N veces por cliente"
        /// </summary>
        /// <param name="discountDTO">Model</param>
        /// <returns></returns>
        [HttpPost("CreateDiscount")]
        public async Task<ActionResult> CreateDiscount([FromBody] DiscountAddDTO discountDTO)
        {
            #region Model Validations
            if (await context.Discount.Where(x => x.Name.Equals(discountDTO.Name)).FirstOrDefaultAsync() is not null)
                return BadRequest("Discount Name already exist.");

            if(discountDTO.CouponCode is not null)
                if (await context.Discount.Where(x => x.CouponCode.Equals(discountDTO.CouponCode)).FirstOrDefaultAsync() is not null)
                    return BadRequest("CouponCode already exist.");

            if (discountDTO.DiscountTypeId == 3 && discountDTO.AppliedToSubCategories is null)
                return BadRequest("AppliedToSubCategories (true/false) is required when DiscountType is Applied to Categories (DiscountTypeId = 3).");

            if (discountDTO.DiscountTypeId != 3 && discountDTO.AppliedToSubCategories is not null)
                return BadRequest("AppliedToSubCategories (true/false) is NOT required when DiscountType is NOT Applied to Categories (DiscountTypeId != 3).");

            if (discountDTO.DiscountLimitationId == 0 && discountDTO.LimitationTimes > 0)
                return BadRequest("LimitationTimes is not required when DiscountLimitationId is unlimited (DiscountTypeId == 0).");

            if (discountDTO.DiscountLimitationId != 0 && discountDTO.LimitationTimes == 0)
                return BadRequest("LimitationTimes is required when DiscountLimitationId is NOT unlimited (DiscountTypeId != 0).");

            if (discountDTO.DiscountPercentage == 0 && discountDTO.DiscountAmount == 0)
                return BadRequest("DiscountPercentage/DiscountAmount, One of them must be grater than 0.");

            if (discountDTO.DiscountPercentage > 0 && discountDTO.DiscountAmount > 0)
                return BadRequest("DiscountPercentage/DiscountAmount, Only one of them must have a value.");

            if (discountDTO.DiscountAmount > 0 && discountDTO.MaximumDiscountAmount > 0)
                return BadRequest("MaximumDiscountAmount is only required when DiscountPercentage is greater than 0");

            if ((discountDTO.DiscountTypeId != 2 && discountDTO.DiscountTypeId != 3) && discountDTO.MaximumDiscountedQuantity > 0)
                return BadRequest("MaximumDiscountedQuantity is NOT required for DiscountTypeId 1 and 4, it is required only for DiscountTypeId 2 or 3 but not mandatory if the value will be 0.");

            if (discountDTO.DiscountTypeId != 2 && discountDTO.DiscountAppliedToProducts is not null)
                return BadRequest("DiscountAppliedToProducts list is NOT required when DiscountType is NOT Applied to Products (DiscountTypeId = 2).");

            if (discountDTO.DiscountTypeId != 3 && discountDTO.DiscountAppliedToCategories is not null)
                return BadRequest("DiscountAppliedToCategories list is NOT required when DiscountType is NOT Applied to Categories (DiscountTypeId = 3).");

            if (discountDTO.DiscountTypeId == 2)
                if (discountDTO.DiscountAppliedToProducts.
                    Where(x => commonMethods.ProductExits(x.ProductId) == true).ToList().Count() < discountDTO.DiscountAppliedToProducts.Count())
                    return BadRequest("Not all of the ProductIds exist.");

            if (discountDTO.DiscountTypeId == 3)
                if (discountDTO.DiscountAppliedToCategories.
                    Where(x => commonCategory.CategoryExist(x.CategoryId) == true).ToList().Count() < discountDTO.DiscountAppliedToCategories.Count())
                    return BadRequest("Not all of the CategoryIds exist.");
            #endregion

            Discount discount = new Discount();
            discount.Name = discountDTO.Name;
            discount.IsCumulative = discountDTO.IsCumulative;

            if (discountDTO.CouponCode is not null)
            {
                discount.CouponCode = discountDTO.CouponCode.ToUpper();
                discount.RequiresCouponCode = true;
            }

            if (discountDTO.DiscountPercentage > 0)
            {
                discount.DiscountPercentage = discountDTO.DiscountPercentage;
                discount.MaximumDiscountAmount = discountDTO.MaximumDiscountAmount;
            }
            else
                discount.DiscountAmount = discountDTO.DiscountAmount;

            switch (discountDTO.DiscountTypeId)
            {
                case 1: //Applied to Total Order
                    discount.DiscountTypeId = 1;
                    break;
                case 2: //Applied to Product
                    discount.DiscountTypeId = 2;
                    discount.MaximumDiscountedQuantity = discountDTO.MaximumDiscountedQuantity;
                    discount.DiscountAppliedToProducts = discountDTO.DiscountAppliedToProducts;
                    break;
                case 3: //Applied to Category
                    discount.DiscountTypeId = 5;
                    discount.AppliedToSubCategories = (bool)discountDTO.AppliedToSubCategories;
                    discount.MaximumDiscountedQuantity = discountDTO.MaximumDiscountedQuantity;
                    discount.DiscountAppliedToCategories = discountDTO.DiscountAppliedToCategories;
                    break;
                case 4: //Applied to Shipping
                    discount.DiscountTypeId = 10;
                    break;
                default:
                    return BadRequest("Invalid DiscountTypeId.");
            }

            switch (discountDTO.DiscountLimitationId)
            {
                case 0: //Unlimited
                    discount.DiscountLimitationId = 0;
                    break;
                case 1: //Limit to N times
                    discount.DiscountLimitationId = 15;
                    discount.LimitationTimes = discountDTO.LimitationTimes;
                    break;
                case 2: //N times per client
                    discount.DiscountLimitationId = 25;
                    discount.LimitationTimes = discountDTO.LimitationTimes;
                    break;
                default:
                    return BadRequest("Invalid DiscountLimitationId.");
            }

            discount.StartDateUtc = discountDTO.StartDateUtc;
            discount.EndDateUtc = discountDTO.EndDateUtc;
            discount.AdminComment = discountDTO.AdminComment;
            
            await context.Discount.AddAsync(discount);
            await context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Elimina un descuento por su Id
        /// </summary>
        /// <param name="discountId">Id de descuento</param>
        /// <returns></returns>
        [HttpDelete("DeleteDiscount/{discountId}")]
        public async Task<ActionResult> DeleteDiscount(int discountId)
        {
            Discount discountToDelete = context.Discount.FirstOrDefault(x => x.Id == discountId);

            if (discountToDelete is null)
                return NotFound("Discount Id not found");
            
            if (discountToDelete.DiscountTypeId == 2)
            {
                List<DiscountAppliedToProducts> productList = context.DiscountAppliedToProducts.Where(x => x.DiscountId == discountId).ToList();
                if(productList.Count > 0)
                    context.DiscountAppliedToProducts.RemoveRange(productList);
            }
            else if (discountToDelete.DiscountTypeId == 5)
            {
                List<DiscountAppliedToCategories> categoryList = context.DiscountAppliedToCategories.Where(x => x.DiscountId == discountId).ToList();
                if(categoryList.Count() > 0)
                context.DiscountAppliedToCategories.RemoveRange(categoryList);
            }

            context.Discount.Remove(discountToDelete);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Relaciona Productos con un descuento
        /// </summary>
        /// <param name="discountAppliedList">Lista del Modelo</param>
        /// <returns></returns>
        [HttpPost("AddProducstToDiscount")]
        public async Task<ActionResult> AddProducstToDiscount([FromBody] List<DiscountAppliedToProductsAddDTO> discountAppliedList)
        {
            #region Model Validation
            if (discountAppliedList.Where(x => commonMethods.ProductExits(x.ProductId) == true).ToList().Count() < discountAppliedList.Count())
                return BadRequest("Not all of the ProductIds exist.");

            List<int> discountIdDistinct = discountAppliedList.Select(x => x.DiscountId).Distinct().ToList();
            List<Discount> existingDiscountIds = context.Discount.ToList().Where(x => discountIdDistinct.Any(y => y == x.Id)).ToList();
            List<Discount> existingFoundDiscountIds = existingDiscountIds.Distinct().ToList();

            if (existingFoundDiscountIds.Count() < discountIdDistinct.Count())
                return BadRequest("One or more DiscountIds wasn't fount or do not exist.");

            if (context.DiscountAppliedToProducts.ToList().
                Where(x => discountAppliedList.Any(y => y.DiscountId == x.DiscountId && y.ProductId == x.ProductId)).ToList().Count() > 0)
                return BadRequest("One or more Products are already related to a Discount.");

            if (existingFoundDiscountIds.Where(x => x.DiscountTypeId != 2).ToList().Count() > 0)
                return BadRequest("One or more DiscountIds are NOT 'Applied to Product' type. The DiscountTypeId must be 2 for each DiscountId.");
            #endregion

            List<DiscountAppliedToProducts> products = mapper.Map<List<DiscountAppliedToProducts>>(discountAppliedList);
            await context.DiscountAppliedToProducts.AddRangeAsync(products);
            await context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Elimina la relación entre un descuento y un producto
        /// </summary>
        /// <param name="discountAppliedToDeleteList"></param>
        /// <returns></returns>
        [HttpDelete("RemoveProductsFromDiscount")]
        public async Task<ActionResult> RemoveProductsFromDiscount(List<DiscountAppliedToProductsAddDTO> discountAppliedToDeleteList)
        {
            #region Model Validation
            if (discountAppliedToDeleteList.Where(x => commonMethods.ProductExits(x.ProductId) == true).ToList().Count() < discountAppliedToDeleteList.Count())
                return BadRequest("Not all of the ProductIds exist.");

            List<int> discountIdDistinct = discountAppliedToDeleteList.Select(x => x.DiscountId).Distinct().ToList();
            List<Discount> existingDiscountIds = context.Discount.ToList().Where(x => discountIdDistinct.Any(y => y == x.Id)).ToList();
            List<Discount> existingFoundDiscountIds = existingDiscountIds.Distinct().ToList();

            if (existingFoundDiscountIds.Count() < discountIdDistinct.Count())
                return BadRequest("One or more DiscountIds wasn't fount or do not exist.");

            if (existingFoundDiscountIds.Where(x => x.DiscountTypeId != 2).ToList().Count() > 0)
                return BadRequest("One or more DiscountIds are NOT 'Applied to Product' type. The DiscountTypeId must be 2 for each DiscountId.");
            #endregion

            List<DiscountAppliedToProducts> toRemoveList = context.DiscountAppliedToProducts.ToList().
               Where(x => discountAppliedToDeleteList.Any(y => y.DiscountId == x.DiscountId && y.ProductId == x.ProductId)).ToList();

            context.DiscountAppliedToProducts.RemoveRange(toRemoveList);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Relaciona Categorias con un descuento
        /// </summary>
        /// <param name="discountAppliedList">Lista del Modelo</param>
        /// <returns></returns>
        [HttpPost("AddCategoriesToDiscount")]
        public async Task<ActionResult> AddCategoryToDiscount([FromBody] List<DiscountAppliedToCategoriesAddDTO> discountAppliedList)
        {
            #region Model Validation
            if (discountAppliedList.Where(x => commonCategory.CategoryExist(x.CategoryId) == true).ToList().Count() < discountAppliedList.Count())
                return BadRequest("Not all of the CategoryIds exist.");

            List<int> discountIdDistinct = discountAppliedList.Select(x => x.DiscountId).Distinct().ToList();
            List<Discount> existingDiscountIds = context.Discount.ToList().Where(x => discountIdDistinct.Any(y => y == x.Id)).ToList();
            List<Discount> existingFoundDiscountIds = existingDiscountIds.Distinct().ToList();

            if (existingFoundDiscountIds.Count() < discountIdDistinct.Count())
                return BadRequest("One or more DiscountIds wasn't fount or do not exist.");

            if (context.DiscountAppliedToCategories.ToList().
                Where(x => discountAppliedList.Any(y => y.DiscountId == x.DiscountId && y.CategoryId == x.CategoryId)).ToList().Count() > 0)
                return BadRequest("One or more Categories are already related to a Discount.");

            if (existingFoundDiscountIds.Where(x => x.DiscountTypeId != 5).ToList().Count() > 0)
                return BadRequest("One or more DiscountIds are NOT 'Applied to Category' type. The DiscountTypeId must be 5 for each DiscountId.");
            #endregion

            List<DiscountAppliedToCategories> categories = mapper.Map<List<DiscountAppliedToCategories>>(discountAppliedList);
            await context.DiscountAppliedToCategories.AddRangeAsync(categories);
            await context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Elimina la relación entre un descuento y una categoría
        /// </summary>
        /// <param name="discountAppliedToDeleteList">Lista del Modelo</param>
        /// <returns></returns>
        [HttpDelete("RemoveCategoryFromDiscount")]
        public async Task<ActionResult> RemoveCategoryFromDiscount(List<DiscountAppliedToCategoriesAddDTO> discountAppliedToDeleteList)
        {
            #region Model Validation
            if (discountAppliedToDeleteList.Where(x => commonCategory.CategoryExist(x.CategoryId) == true).ToList().Count() < discountAppliedToDeleteList.Count())
                return BadRequest("Not all of the CategoryIds exist.");

            List<int> discountIdDistinct = discountAppliedToDeleteList.Select(x => x.DiscountId).Distinct().ToList();
            List<Discount> existingDiscountIds = context.Discount.ToList().Where(x => discountIdDistinct.Any(y => y == x.Id)).ToList();
            List<Discount> existingFoundDiscountIds = existingDiscountIds.Distinct().ToList();

            if (existingFoundDiscountIds.Count() < discountIdDistinct.Count())
                return BadRequest("One or more DiscountIds wasn't fount or do not exist.");

            if (existingFoundDiscountIds.Where(x => x.DiscountTypeId != 5).ToList().Count() > 0)
                return BadRequest("One or more DiscountIds are NOT 'Applied to Category' type. The DiscountTypeId must be 5 for each DiscountId.");
            #endregion

            List<DiscountAppliedToCategories> toRemoveList = context.DiscountAppliedToCategories.ToList().
               Where(x => discountAppliedToDeleteList.Any(y => y.DiscountId == x.DiscountId && y.CategoryId == x.CategoryId)).ToList();

            context.DiscountAppliedToCategories.RemoveRange(toRemoveList);
            await context.SaveChangesAsync();

            return Ok();
        }
    }
}
