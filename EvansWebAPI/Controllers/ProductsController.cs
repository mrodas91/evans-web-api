﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EvansWebAPI.Contexts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EvansWebAPI.Entities;
using EvansWebAPI.Models;
using Microsoft.Extensions.Options;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using EvansWebAPI.Common;
using EvansWebAPI.EntityLanguages;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EvansWebAPI.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProductsController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IOptions<ApiBehaviorOptions> apiBehaviorOptions;
        private readonly IConfiguration configuration;
        private readonly CommonMethods commonMethods;
        private readonly CommonCategory commonCategory;

        public ProductsController(ApplicationDbContext context, IMapper mapper, IOptions<ApiBehaviorOptions> apiBehaviorOptions,
            IConfiguration configuration, CommonMethods commonMethods, CommonCategory commonCategory)
        {
            this.context = context;
            this.mapper = mapper;
            this.apiBehaviorOptions = apiBehaviorOptions;
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            this.commonMethods = commonMethods;
            this.commonCategory = commonCategory;
        }

        /// <summary>
        /// Obtiene información basica de un producto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetProductById/{id}", Name = "GetProductById")]
        public async Task<ActionResult<ProductInfoDTO>> GetProductById(int id)
        {

            Product product = await context.Product.FirstOrDefaultAsync(x => x.Id == id);

            if (product == null)
                return NotFound();

            var productDTO = mapper.Map<ProductInfoDTO>(product);

            return productDTO;
        }

        /// <summary>
        /// Agrega un nuevo producto
        /// </summary>
        /// <param name="productDTO"></param>
        /// <returns></returns>
        [HttpPost("AddProduct")]
        public async Task<ActionResult> AddProduct([FromBody] ProductAddDTO productDTO)
        {
            Product product = mapper.Map<Product>(productDTO);

            #region SKU Validation

            Product existingSKU = context.Product.FirstOrDefault(x => x.Sku == product.Sku);
            if (existingSKU is not null)
            {
                ModelState.AddModelError("SKU", "The SKU is already in use by other product");
                return (ActionResult)apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
            }
            #endregion

            #region Category Validation
            if (productDTO.ProductCategoryMapping is not null)
            {
                ProductCategoryMapping productCategory = product.ProductCategoryMapping.FirstOrDefault();
                int valResult = commonCategory.ValidateProductCategoryRelation(productCategory.CategoryId);

                if (valResult != 0)
                {
                    if (valResult == 1)
                        return BadRequest("The category related to the product was not found or does not exist.");
                    else if (valResult == 2)
                        return BadRequest("A product can't be related to root category (MegaFamilia).");
                    else if (valResult == 3)
                        return BadRequest("A product can't be related to a Familia category.");
                }
            }
            #endregion

            #region Other default values
            //Don't Track Inventory
            product.ManageInventoryMethodId = 0;

            if (product.OrderMinimumQuantity == 0)
            {
                product.OrderMinimumQuantity = 1;
                product.OrderMaximumQuantity = 10000;
            }

            if (product.OrderMaximumQuantity == 0)
                product.OrderMaximumQuantity = 10000;

            if (!product.IsTaxExempt)
                product.TaxCategoryId = Convert.ToInt32(configuration["DefaultDbValues:TaxCategoryId"]);

            product.MetaTitle = product.Name;
            product.AllowCustomerReviews = true;
            product.VisibleIndividually = true;
            product.ProductTemplateId = 1;
            product.CreatedOnUtc = DateTime.UtcNow;
            product.UpdatedOnUtc = product.CreatedOnUtc;
            product.ProductTypeId = 1;
            product.IsShipEnabled = true;
            product.IsDownload = false;
            #endregion

            await context.Product.AddAsync(product);
            await context.SaveChangesAsync();

            if (product.Id != 0)
            {
                #region URL Settings
                await commonMethods.CreateUrlRecord("Product", product.Id, product.Name, Convert.ToInt32(configuration["DefaultDbValues:SpanishLanguageId"]));
                #endregion

                #region Language Settings
                if (product.ProductLanguageValues is not null)
                    await commonMethods.AddEnglishValues(product.ProductLanguageValues, "Product", product.Id);
                #endregion

                #region Adding Picture
                if (productDTO.PicturePaths is not null)
                {
                    int order = 0;
                    foreach (string path in productDTO.PicturePaths)
                    {
                        order += 1;
                        await commonMethods.AddProductPicture(path, product, order);
                    }
                }
                #endregion
            }

            var shortProductInfo = mapper.Map<ProductShortInfoDTO>(product);

            return new CreatedAtRouteResult("GetProductById", new { id = product.Id }, shortProductInfo);
        }

        /// <summary>
        /// Agrega un producto a una Categoria
        /// </summary>
        /// <param name="categoryDTO">Modelo</param>
        /// <returns></returns>
        [HttpPost("AddProductToCategory")]
        public async Task<ActionResult> AddProductToCategory([FromBody] ProductCategoryMappingAddDTO categoryDTO)
        {
            int valResult = commonCategory.ValidateProductCategoryRelation(categoryDTO.CategoryId);
            if (valResult != 0)
            {
                if (valResult == 1)
                    return BadRequest("The category related to the product was not found or does not exist.");
                else if (valResult == 2)
                    return BadRequest("A product can't be related to root category (MegaFamilia).");
                else if (valResult == 3)
                    return BadRequest("A product can't be related to a Familia category.");
            }
            if (!commonMethods.ProductExits(categoryDTO.ProductId))
                return BadRequest("The ProductId wasn't sent or it does not exist.");
            if (context.ProductCategoryMapping.ToList().Where(x => x.CategoryId == categoryDTO.CategoryId && x.ProductId == categoryDTO.ProductId).ToList().Count() > 0)
                return BadRequest("The ProductId is already related to this Category.");

            ProductCategoryMapping categoryMapping = mapper.Map<ProductCategoryMapping>(categoryDTO);

            await context.AddAsync(categoryMapping);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Agrega una serie de Productos a una Colección
        /// </summary>
        /// <param name="categoriesDTO">Lista del Modelo</param>
        /// <returns></returns>
        [HttpPost("AddProductToCategoryBulk")]
        public async Task<ActionResult> AddProductToCategoryBulk([FromBody] List<ProductCategoryMappingAddDTO> categoriesDTO)
        {
            List<ProductCategoryMappingAddDTO> x = categoriesDTO.Where(x => commonCategory.IsCollection(x.CategoryId) == false).ToList();

            if (categoriesDTO.Where(x => commonCategory.CategoryExist(x.CategoryId) == true).ToList().Count() < categoriesDTO.Count())
                return BadRequest("Not all of the CategoryIds exist.");
            if (categoriesDTO.Where(x => commonMethods.ProductExits(x.ProductId) == true).ToList().Count() < categoriesDTO.Count())
                return BadRequest("Not all of the ProductIds exist or they were not sent.");
            if (context.ProductCategoryMapping.ToList().Where(x => categoriesDTO.Any(y => x.CategoryId == y.CategoryId && x.ProductId == y.ProductId)).ToList().Count() > 0)
                return BadRequest("One or more ProductIds are already related to a Category.");
            if (categoriesDTO.Where(x => commonCategory.IsMegaFamilia(x.CategoryId) == true).ToList().Count() > 0)
                return BadRequest("One or more products are trying to be related to root category (MegaFamilia).");
            if (categoriesDTO.Where(x => commonCategory.IsFamilia(x.CategoryId) == true).ToList().Count() > 0)
                return BadRequest("One or more products are trying to be related to Family category.");

            List<ProductCategoryMapping> categoriesMapping = mapper.Map<List<ProductCategoryMapping>>(categoriesDTO);

            await context.AddRangeAsync(categoriesMapping);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Agrega agrega una lista de imagenes a uno o mas productos
        /// </summary>
        /// <param name="productPictureDTO">Model</param>
        /// <returns></returns>
        [HttpPost("AddProductPicture")]
        public async Task<ActionResult> AddProductPictures([FromBody] List<ProductPictureAdd> productPictureDTO)
        {
            if (productPictureDTO.Where(x => commonMethods.ProductExits((int)x.ProductId) == true).ToList().Count() < productPictureDTO.Count())
                return BadRequest("Not all of the ProductIds exist.");

            List<Product> productList = context.Product.ToList()
                .Where(x => productPictureDTO.Any(y => x.Id == y.ProductId))
                .ToList().Distinct().ToList();

            if (!productList.Any())
                return NotFound("No results found.");

            int order = 0;
            foreach(ProductPictureAdd picturePath in productPictureDTO)
            {
                order += 1;
                Product product = productList.FirstOrDefault(x => x.Id == picturePath.ProductId);
                await commonMethods.AddProductPicture(picturePath.LocalPath, product, order);
            }   

            return Ok();
        }

        /// <summary>
        /// Agrega los valores en inglés para Productos
        /// </summary>
        /// <param name="languageValuesDTO"></param>
        /// <returns></returns>
        [HttpPut("AddProductLangageValues")]
        public async Task<ActionResult> AddProductLangageValues([FromBody] List<ProductLanguageValuesDTO> languageValuesDTO)
        {
            if (languageValuesDTO.Where(x => x.EntityId == null).ToList().Count() > 0)
                return BadRequest("EntityId is required for all of the elements in the list.");
            if (languageValuesDTO.Where(x => commonMethods.ProductExits((int)x.EntityId) == true).ToList().Count() < languageValuesDTO.Count())
                return BadRequest("Not all of the ProductIds exist.");

            List<DefaultLanguageValues> languageValuesList = mapper.Map<List<DefaultLanguageValues>>(languageValuesDTO);
            await commonMethods.AddEnglishValues(languageValuesList, "Product");

            return Ok();
        }

        /// <summary>
        /// Remueve una serie de productos de una o varias Categorias
        /// </summary>
        /// <param name="categoriesDTO"></param>
        /// <returns></returns>
        [HttpDelete("RemoveProductFromCategoryBulk")]
        public async Task<ActionResult> RemoveProductFromCategoryBulk([FromBody] List<ProductCategoryMappingRemoveDTO> categoriesDTO)
        {
            if (categoriesDTO.Where(x => commonCategory.CategoryExist(x.CategoryId) == true).ToList().Count() < categoriesDTO.Count())
                return BadRequest("Not all of the CategoryIds exist.");
            if (categoriesDTO.Where(x => commonMethods.ProductExits(x.ProductId) == true).ToList().Count() < categoriesDTO.Count())
                return BadRequest("Not all of the ProductIds exist.");

            List<ProductCategoryMapping> categoriesMappingToRemove = context.ProductCategoryMapping.ToList()
                    .Where(x => categoriesDTO.Any(y => x.CategoryId == y.CategoryId && x.ProductId == y.ProductId)).ToList();

            if (categoriesMappingToRemove.Count() == 0)
                return NotFound("No values were found.");

            context.RemoveRange(categoriesMappingToRemove);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Elimina un producto de una Categoría
        /// </summary>
        /// <param name="categoryDTO">Modelo</param>
        /// <returns></returns>
        [HttpDelete("RemoveProductFromCategory")]
        public async Task<ActionResult> RemoveProductFromCategory([FromBody] ProductCategoryMappingRemoveDTO categoryDTO)
        {
            if (!commonCategory.CategoryExist(categoryDTO.CategoryId))
                return BadRequest("The CategoryId does not exist.");
            if (!commonMethods.ProductExits(categoryDTO.ProductId))
                return BadRequest("The ProductId does not exist.");

            ProductCategoryMapping categoryMappingToRemove = await context.ProductCategoryMapping.
                FirstOrDefaultAsync(x => x.CategoryId == categoryDTO.CategoryId && x.ProductId == categoryDTO.ProductId);

            if (categoryMappingToRemove is null)
                return BadRequest("No values found.");

            context.Remove(categoryMappingToRemove);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Habilita o desabilita un producto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("EnableDisableProduct/{id}")]
        public async Task<ActionResult> EnableDisableProduct(int id)
        {
            if (!commonMethods.ProductExits(id))
                return BadRequest("The ProductId does not exist.");

            var product = await context.Product.FirstOrDefaultAsync(x => x.Id == id);
            string returnMsg = string.Empty;

            if (product.Published)
            {
                product.Published = false;
                returnMsg = "Product disable.";
            }
            else
            {
                product.Published = true;
                returnMsg = "Product enable.";
            }
            await context.SaveChangesAsync();
            await commonMethods.EnableDisableUrlRecords("Product", product.Id);
            
            return Ok();
        }

        /// <summary>
        /// Muestra/Oculta un producto de la pagina de inicio
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("ShowHideOnHomePage/{id}")]
        public async Task<ActionResult> ShowHideOnHomePage(int id)
        {
            if (!commonMethods.ProductExits(id))
                return BadRequest("The ProductId does not exist.");

            var product = await context.Product.FirstOrDefaultAsync(x => x.Id == id);
            string returnMsg = string.Empty;

            if (product.Published)
            {
                product.ShowOnHomepage = false;
                returnMsg = "Product hiden.";
            }
            else
            {
                product.ShowOnHomepage = true;
                returnMsg = "Product shown.";
            }
            await context.SaveChangesAsync();
            return Ok();
        }

        #region Product Related
        /// <summary>
        /// Busca y retorna el id de los productos relacionados al Id del producto enviado
        /// </summary>
        /// <param name="originProductId">Id del producto a buscar</param>
        /// <returns></returns>
        [HttpGet("GetRelatedProductsById/{originProductId}")]
        public async Task<ActionResult<List<RelatedProductDTO>>> GetRelatedProductsById(int originProductId)
        {
            List<RelatedProduct> relatedProductsList = await context.RelatedProduct.Where(x => x.ProductId1 == originProductId).ToListAsync();

            if (relatedProductsList.Count == 0)
                return NotFound("There aren't any products related.");

            List<RelatedProductDTO> relatedproductsDTO = new List<RelatedProductDTO>();

            relatedProductsList.ForEach(x => relatedproductsDTO.Add(new RelatedProductDTO
            {
                ProductOriginId = x.ProductId1,
                ProductToRelateId = x.ProductId2
            }));

            return relatedproductsDTO;
        }

        /// <summary>
        /// Relaciona dos productos
        /// </summary>
        /// <param name="relatedProduct"></param>
        /// <returns></returns>
        [HttpPost("AddRelatedProduct")]
        public async Task<ActionResult> AddRelatedProduct([FromBody] RelatedProductDTO relatedProduct)
        {
            #region Product Existing Validation
            if (context.Product.Where(x => x.Id.Equals(relatedProduct.ProductOriginId)).FirstOrDefault() is null)
                return BadRequest("Product Origin was not fount or do not exist");

            else if (context.Product.Where(x => x.Id.Equals(relatedProduct.ProductToRelateId)).FirstOrDefault() is null)
                return BadRequest("Product to Relate was not fount or do not exist.");

            else if (context.RelatedProduct.Where(x => x.ProductId1 == relatedProduct.ProductOriginId &&
                x.ProductId2 == relatedProduct.ProductToRelateId).ToList().Count() > 0)
                return BadRequest("These Products are already related.");
            #endregion

            RelatedProduct product = new RelatedProduct
            {
                ProductId1 = relatedProduct.ProductOriginId,
                ProductId2 = relatedProduct.ProductToRelateId
            };

            await context.RelatedProduct.AddAsync(product);
            await context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Relaciona dos productos de forma masiva
        /// </summary>
        /// <param name="relatedProductsDTO"></param>
        /// <returns></returns>
        [HttpPost("AddRelatedProductBulk")]
        public async Task<ActionResult> AddRelatedProductBulk([FromBody] List<RelatedProductDTO> relatedProductsDTO)
        {
            #region Product Existing Validation
            List<int> origingProdIdDistinct = relatedProductsDTO.Select(x => x.ProductOriginId).Distinct().ToList();
            List<Product> productsOriginList = context.Product.ToList()
                .Where(x => origingProdIdDistinct.Any(y => x.Id == y)).ToList();
            List<Product> origingProdFoundDistinct = productsOriginList.Distinct().ToList();

            if (origingProdFoundDistinct.Count < origingProdIdDistinct.Count)
                return BadRequest("One or more Origin Product(s) wasn't fount or do not exist.");

            List<int> prodToRelateIdDistinct = relatedProductsDTO.Select(x => x.ProductToRelateId).Distinct().ToList();
            List<Product> productsToRelateList = context.Product.ToList()
                .Where(x => prodToRelateIdDistinct.Any(y => x.Id == y)).ToList();
            List<Product> ProdToRelateFoundDistinct = productsToRelateList.Distinct().ToList();

            if (ProdToRelateFoundDistinct.Count < prodToRelateIdDistinct.Count)
                return BadRequest("One or more Product(s) to Relate wasn't fount or do not exist.");

            List<RelatedProduct> relatedProductsList = context.RelatedProduct.ToList()
                .Where(x => relatedProductsDTO.Any(y => x.ProductId1 == y.ProductOriginId && x.ProductId2 == y.ProductToRelateId)).ToList();

            if(relatedProductsList.Count() > 0)
                return BadRequest("One or more Products are already related.");
            #endregion

            List<RelatedProduct> products = new List<RelatedProduct>();

            relatedProductsDTO.ForEach(x => products.Add(new RelatedProduct
            {
                ProductId1 = x.ProductOriginId,
                ProductId2 = x.ProductToRelateId
            }));  

            await context.RelatedProduct.AddRangeAsync(products);
            await context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Elimina la relación entre dos productos
        /// </summary>
        /// <param name="relatedProductToDelete">Modelo</param>
        /// <returns></returns>
        [HttpDelete("DeleteRelatedProduct")]
        public async Task<ActionResult> DeleteRelatedProduct([FromBody] RelatedProductDTO relatedProductToDelete)
        {
            var relatedProduct = await context.RelatedProduct.
                FirstOrDefaultAsync(x => x.ProductId1 == relatedProductToDelete.ProductOriginId && x.ProductId2 == relatedProductToDelete.ProductToRelateId);
            
            if (relatedProduct is null)
                return NotFound();

            context.Remove(relatedProduct);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Elimina la relación entre dos productos de forma masiva
        /// </summary>
        /// <param name="relatedProductsToDelete">Lista del Modelo</param>
        /// <returns></returns>
        [HttpDelete("DeleteRelatedProductBulk")]
        public async Task<ActionResult> DeleteRelatedProductBulk([FromBody] List<RelatedProductDTO> relatedProductsToDelete)
        {
            List<RelatedProduct> relatedProducts = context.RelatedProduct.ToList()
                .Where(x => relatedProductsToDelete.Any(y => x.ProductId1 == y.ProductOriginId && x.ProductId2 == y.ProductToRelateId))
                .ToList();

            if (relatedProducts.Count() == 0)
                return NotFound("No results found.");
            else if (relatedProducts.Count < relatedProductsToDelete.Count)
                return BadRequest("Not all the elements sent in the model were found in the Db, please check you are using the correct Ids.");

            context.RemoveRange(relatedProducts);
            await context.SaveChangesAsync();

            return Ok();
        }
        #endregion

        #region Product Specifications
        /// <summary>
        /// Agrega un nuevo tipo de descripción del producto
        /// </summary>
        /// <param name="name">Nombre del tipo</param>
        /// <returns></returns>
        [HttpPost("AddProductDescriptionType/{name}/{enName?}")]
        public async Task<ActionResult> AddProductDescriptionType(string name, string enName)
        {
            if(String.IsNullOrWhiteSpace(name))
                return BadRequest("Name can't be empty.");
            if (name.Length >= 30)
                return BadRequest("Name length can't be more than 30.");
            if (context.ProductDescriptionType.Where(x => x.Name == name).ToList().Count() > 0)
                return BadRequest("The name you are trying to insert already exist in the Database.");

            ProductDescriptionType type = new ProductDescriptionType { Name = name };

            await context.ProductDescriptionType.AddAsync(type);
            await context.SaveChangesAsync();

            if (!string.IsNullOrWhiteSpace(enName))
                await commonMethods.AddEnglishValues(new DefaultLanguageValues { EnName = enName }, "ProductDescriptionType", type.Id);

            return Ok();
        }

        /// <summary>
        /// Crea un Atributo de especificación de un producto
        /// </summary>
        /// <param name="name">Nombre de la descripción</param>
        /// <returns></returns>
        [HttpPost("AddSpecificationAttribute")]
        public async Task<ActionResult> AddSpecificationAttribute(GenericLanguajeValueDTO names)
        {
            if (context.SpecificationAttribute.Where(x => x.Name == names.Value).ToList().Count() > 0)
                return BadRequest("The name you are trying to insert already exist in the Database.");

            SpecificationAttribute attribute = new SpecificationAttribute { Name = names.Value };
            await context.SpecificationAttribute.AddAsync(attribute);
            await context.SaveChangesAsync();

            #region Language Settings
            if (!string.IsNullOrWhiteSpace(names.EnValue))
                await commonMethods.AddEnglishValues(new DefaultLanguageValues { EnName = names.EnValue }, "SpecificationAttribute", attribute.Id);
            #endregion

            return Ok();
        }

        /// <summary>
        /// Crea una serie Atributos de especificación de un producto
        /// </summary>
        /// <param name="namesDTO">Lista de nombres</param>
        /// <returns></returns>
        [HttpPost("AddSpecificationAttributeBulk")]
        public async Task<ActionResult> AddSpecificationAttributeBulk([FromBody] List<GenericLanguajeValueDTO> namesDTO)
        {
            if (context.SpecificationAttribute.ToList().Where(x => namesDTO.Any(y => x.Name == y.Value)).ToList().Count() > 0)
                return BadRequest("One or more names in the list already exist in the Database.");

            List<SpecificationAttribute> attributesList = new List<SpecificationAttribute>();
            namesDTO.ForEach(i => attributesList.Add(new SpecificationAttribute { Name = i.Value }));

            await context.SpecificationAttribute.AddRangeAsync(attributesList);
            await context.SaveChangesAsync();

            #region Lenguage Settings
            List<DefaultLanguageValues> values = new List<DefaultLanguageValues>();
            attributesList.ForEach(x => namesDTO.Where(y => y.Value == x.Name).ToList().
                ForEach(z => values.Add(new DefaultLanguageValues { EntityId = x.Id, EnName = z.EnValue })));

            await commonMethods.AddEnglishValues(values, "SpecificationAttribute");
            #endregion

            return Ok();
        }

        /// <summary>
        /// Crea/Relaciona una opcion(valor) con un attibuto de producto  
        /// </summary>
        /// <param name="optionDTO"></param>
        /// <returns></returns>
        [HttpPost("AddSpecificationAttributeOption")]
        public async Task<ActionResult> AddSpecificationAttributeOption([FromBody] SpecificationAttributeOptionAddDTO optionDTO)
        {
            if (context.SpecificationAttributeOption.
                Where(x => x.Name == optionDTO.Name && x.SpecificationAttributeId == optionDTO.SpecificationAttributeId).ToList().Count() > 0)
                return BadRequest("The option you are trying to insert already exist in the Database.");

            SpecificationAttributeOption attributeOption = mapper.Map<SpecificationAttributeOption>(optionDTO);
            await context.SpecificationAttributeOption.AddAsync(attributeOption);
            await context.SaveChangesAsync();
            
            if (attributeOption.DefaultLanguageValues is not null)
                await commonMethods.AddEnglishValues(attributeOption.DefaultLanguageValues, "SpecificationAttributeOption", attributeOption.Id);
            return Ok();
        }

        /// <summary>
        /// Crea/Relaciona opciones(valores) de especificaciones con attibutos de producto
        /// </summary>
        /// <param name="optionDTO"></param>
        /// <returns></returns>
        [HttpPost("AddSpecificationAttributeOptionBulk")]
        public async Task<ActionResult> AddSpecificationAttributeOptionBulk([FromBody] List<SpecificationAttributeOptionAddDTO> optionDTO)
        {
            if (context.SpecificationAttributeOption.ToList().
                Where(x => optionDTO.Any(y => x.Name == y.Name && x.SpecificationAttributeId == y.SpecificationAttributeId)).ToList().Count() > 0)
                return BadRequest("One or more options you are trying to insert already exist in the Database.");

            List<SpecificationAttributeOption> attributeOptions = mapper.Map<List<SpecificationAttributeOption>>(optionDTO);
            await context.SpecificationAttributeOption.AddRangeAsync(attributeOptions);
            await context.SaveChangesAsync();

            List<DefaultLanguageValues> values = new List<DefaultLanguageValues>();
            attributeOptions.ForEach(x => values.Add(new DefaultLanguageValues { EntityId = x.Id, EnName = x.DefaultLanguageValues.EnName }));
            await commonMethods.AddEnglishValues(values, "SpecificationAttributeOption");
            return Ok();
        }

        /// <summary>
        /// Agrega los valores en inglés para ProductDescriptionType
        /// </summary>
        /// <param name="languageValuesDTO"></param>
        /// <returns></returns>
        [HttpPut("AddProductDescriptionTypeLangageValues")]
        public async Task<ActionResult> AddProductDescriptionTypeLangageValues([FromBody] List<DefaultLanguageValueDTO> languageValuesDTO)
        {
            if (languageValuesDTO.Where(x => x.EntityId == null).ToList().Count() > 0)
                return BadRequest("EntityId is required for all of the elements in the list.");
            if (context.ProductDescriptionType.ToList().Where(x => languageValuesDTO.Any(y => x.Id == y.EntityId)).ToList().Count() < languageValuesDTO.Count())
                return BadRequest("Not all of the EntityIds exist for ProductDescriptionType in the Database.");

            List<DefaultLanguageValues> languageValuesList = mapper.Map<List<DefaultLanguageValues>>(languageValuesDTO);
            await commonMethods.AddEnglishValues(languageValuesList, "ProductDescriptionType");

            return Ok();
        }

        /// <summary>
        /// Agregar los valores en inglés para SpecificationAttribute
        /// </summary>
        /// <param name="languageValuesDTO"></param>
        /// <returns></returns>
        [HttpPut("AddSpecificationAttributeLangageValues")]
        public async Task<ActionResult> AddSpecificationAttributeLangageValues([FromBody] List<DefaultLanguageValueDTO> languageValuesDTO)
        {
            if (languageValuesDTO.Where(x => x.EntityId == null).ToList().Count() > 0)
                return BadRequest("EntityId is required for all of the elements in the list.");
            if (context.SpecificationAttribute.ToList().Where(x => languageValuesDTO.Any(y => x.Id == y.EntityId)).ToList().Count() < languageValuesDTO.Count())
                return BadRequest("Not all of the EntityIds exist for ProductDescriptionType in the Database.");

            List<DefaultLanguageValues> languageValuesList = mapper.Map<List<DefaultLanguageValues>>(languageValuesDTO);
            await commonMethods.AddEnglishValues(languageValuesList, "SpecificationAttribute");

            return Ok();
        }

        /// <summary>
        /// Agregar los valores en inglés para SpecificationAttributeOption
        /// </summary>
        /// <param name="languageValuesDTO"></param>
        /// <returns></returns>
        [HttpPut("AddSpecificationAttributeOptionLangageValues")]
        public async Task<ActionResult> AddSpecificationAttributeOptionLangageValues([FromBody] List<DefaultLanguageValueDTO> languageValuesDTO)
        {
            if (languageValuesDTO.Where(x => x.EntityId == null).ToList().Count() > 0)
                return BadRequest("EntityId is required for all of the elements in the list.");
            if (context.SpecificationAttributeOption.ToList().Where(x => languageValuesDTO.Any(y => x.Id == y.EntityId)).ToList().Count() < languageValuesDTO.Count())
                return BadRequest("Not all of the EntityIds exist for ProductDescriptionType in the Database.");

            List<DefaultLanguageValues> languageValuesList = mapper.Map<List<DefaultLanguageValues>>(languageValuesDTO);
            await commonMethods.AddEnglishValues(languageValuesList, "SpecificationAttributeOption");

            return Ok();
        }


        /// <summary>
        /// Devuelve la lista de especificaciones relacionadas a un producto
        /// </summary>
        /// <param name="productId">Id del producto</param>
        /// <returns></returns>
        [HttpGet("GetProductSpecificationAttributes/{productId}")]
        public async Task<ActionResult<List<ProductSpecificationAttributesDTO>>> GetProductSpecificationAttributes(int productId)
        {
            List<ProductSpecificationAttributesDTO> productSpecAttributes = await
                context.ProductSpecificationAttributeMapping.
                Include(x => x.SpecificationAttributeOption).ThenInclude(x => x.SpecificationAttribute).
                Include(x => x.Product).
                Include(x => x.ProductDescriptionType).
                Where(x => x.ProductId == productId).
                Select(x => new ProductSpecificationAttributesDTO
                {
                    Id = x.Id,
                    DisplayOrder = x.DisplayOrder,
                    ProductDescriptionTypeId = x.ProductDescriptionTypeId,
                    Product = mapper.Map<ProductShortInfoDTO>(x.Product),
                    SpecificationAttributeOption = mapper.Map<SpecificationAttributeOptionInfoDTO>(x.SpecificationAttributeOption),
                    ProductDescriptionType = mapper.Map<ProductDescriptionTypeInfoDTO>(x.ProductDescriptionType)
                }).OrderBy(x => x.ProductDescriptionTypeId)
                .ThenBy(x => x.DisplayOrder).ToListAsync();

            if (productSpecAttributes == null)
                return NotFound("No results found for Product Id: " + productId);

            return productSpecAttributes;
        }

        /// <summary>
        /// Relaciona un producto con una opcion de especificación.
        /// </summary>
        /// <param name="productSpecAttributeDTO">Modelo</param>
        /// <returns></returns>
        [HttpPost("RelateProductSpecificationAttributes")]
        public async Task<ActionResult> AddProductDescriptionValue([FromBody] ProductSpecificationAttributesAddDTO productSpecAttributeDTO)
        {
            if (context.ProductSpecificationAttributeMapping.
                Where(x => x.ProductId == productSpecAttributeDTO.ProductId &&
                            x.ProductDescriptionTypeId == productSpecAttributeDTO.ProductDescriptionTypeId &&
                            x.SpecificationAttributeOptionId == productSpecAttributeDTO.SpecificationAttributeOptionId).ToList().Count() > 0)
                return BadRequest("This relation already exist.");

            ProductSpecificationAttributeMapping productSpecAttribute = mapper.Map<ProductSpecificationAttributeMapping>(productSpecAttributeDTO);
            productSpecAttribute.ShowOnProductPage = true;

            await context.ProductSpecificationAttributeMapping.AddAsync(productSpecAttribute);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Relaciona una serie de productos con una opcion de especificación.
        /// </summary>
        /// <param name="productSpecAttributesDTO">Lista del Modelo</param>
        /// <returns></returns>
        [HttpPost("RelateProductSpecificationAttributesBulk")]
        public async Task<ActionResult> RelateProductSpecificationAttributesBulk([FromBody] List<ProductSpecificationAttributesAddDTO> productSpecAttributesDTO)
        {
            if(context.ProductSpecificationAttributeMapping.ToList().
                Where(x =>
                productSpecAttributesDTO.Any(y => y.ProductId == x.ProductId &&
                                                    y.ProductDescriptionTypeId == x.ProductDescriptionTypeId &&
                                                    y.SpecificationAttributeOptionId == x.SpecificationAttributeOptionId)).ToList().Count() > 0)
                return BadRequest("There are one or more relation that already exist.");


            List<ProductSpecificationAttributeMapping> attributeMappingList = mapper.Map<List<ProductSpecificationAttributeMapping>>(productSpecAttributesDTO);

            attributeMappingList.ForEach(x => x.ShowOnProductPage = true);

            await context.ProductSpecificationAttributeMapping.AddRangeAsync(attributeMappingList);
            await context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Elimina la relación entre un producto y una especificaión
        /// </summary>
        /// <param name="specAttibutesToDeleteDTO">Modelo</param>
        /// <returns></returns>
        [HttpDelete("DeleteProductSpecificationAttributesRelation")]
        public async Task<ActionResult> DeleteProductSpecificationAttributesRelation([FromBody] ProductSpecificationAttributesRemoveDTO specAttibutesToDeleteDTO)
        {
            ProductSpecificationAttributeMapping productSpecAttribute = await
                context.ProductSpecificationAttributeMapping.Where(x =>
                            x.ProductDescriptionTypeId == specAttibutesToDeleteDTO.ProductDescriptionTypeId &&
                            x.SpecificationAttributeOptionId == specAttibutesToDeleteDTO.SpecificationAttributeOptionId &&
                            x.ProductId == specAttibutesToDeleteDTO.ProductId).FirstOrDefaultAsync();

            if (productSpecAttribute is null)
                return NotFound("No ralation found or it does not exist.");

            context.Remove(productSpecAttribute);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Elimina una serie de relaciones entre un productos y especificaines
        /// </summary>
        /// <param name="specAttibutesToDeleteDTO">Modelo</param>
        /// <returns></returns>
        [HttpDelete("DeleteProductSpecificationAttributesRelationBulk")]
        public async Task<ActionResult> DeleteProductSpecificationAttributesRelationBulk([FromBody] List<ProductSpecificationAttributesRemoveDTO> specAttibutesToDeleteDTO)
        {
            List<ProductSpecificationAttributeMapping> productSpecAttribute = 
                context.ProductSpecificationAttributeMapping.ToList().Where(x => specAttibutesToDeleteDTO.Any(y =>
                            x.ProductDescriptionTypeId == y.ProductDescriptionTypeId &&
                            x.SpecificationAttributeOptionId == y.SpecificationAttributeOptionId &&
                            x.ProductId == y.ProductId)).ToList();

            if (productSpecAttribute.Count() < specAttibutesToDeleteDTO.Count())
                return NotFound("Not all ralations were found.");

            context.RemoveRange(productSpecAttribute);
            await context.SaveChangesAsync();

            return Ok();
        }

        #endregion

        #region Product Documents

        /// <summary>
        /// Obtiene todos los tipos de documentos
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetProductsDocType")]
        public async Task<ActionResult<List<ProductDocumentType>>> GetProductDocumentType()
        {
            List<ProductDocumentType> productDoctosType = await
                context.ProductDocumentType.ToListAsync();

            if (productDoctosType == null)
                return NotFound("No results found.");

            return productDoctosType;
        }

        /// <summary>
        /// Agregar tipos de documentos que se relacionaran con el producto. Ej. Manual de Usuario
        /// </summary>
        /// <param name="name">Nombre del tipo de documento</param>
        /// <returns></returns>
        [HttpPost("AddProductDocumentType/{name}")]
        public async Task<ActionResult> AddProductDocumentType(string name)
        {
            if (name.Length > 30)
            {
                ModelState.AddModelError(string.Empty, "Name length can't be more than 30.");
                return (ActionResult)apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
            }

            ProductDocumentType type = new ProductDocumentType { Name = name };

            await context.ProductDocumentType.AddAsync(type);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Actualizar nombre de un tipo de documento
        /// </summary>
        /// <param name="id">Id del tipo de documento</param>
        /// <param name="newName">Nuevo nombre</param>
        /// <returns></returns>
        [HttpPut("UpdateProductDocumentType/{id}/{newName}")]
        public async Task<ActionResult> UpdateProductDocumentType(int id, string newName)
        {
            if (newName.Length > 30)
            {
                ModelState.AddModelError(string.Empty, "Name length can't be more than 30.");
                return (ActionResult)apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
            }

            var productDoctoType = await context.ProductDocumentType.
                FirstOrDefaultAsync(x => x.Id == id);

            if (productDoctoType is null)
                return NotFound("Id " + id + " Not found.");
            
            productDoctoType.Name = newName;
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Elimina un tipo de documento de producto
        /// </summary>
        /// <param name="id">Id del documento</param>
        /// <returns></returns>
        [HttpDelete("DeleteProductDocumentType/{id}")]
        public async Task<ActionResult> DeleteProductDocument(int id)
        {                       
            var productDoctoType = await context.ProductDocumentType.FirstOrDefaultAsync(x => x.Id == id);
            
            if (productDoctoType is null)
                return NotFound("Id " + id + " Not found.");

            context.Remove(productDoctoType);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Obtiene todos los documentos relacionados al producto especificado
        /// </summary>
        /// <param name="productId">Id del producto a consultar</param>
        /// <returns></returns>
        [HttpGet("GetProductDocument/{productId?}")]
        public async Task<ActionResult<List<ProductDocumentGetDTO>>> GetProductDocument(int productId)
        {
            List<ProductDocumentGetDTO> productDoctos = await
                context.ProductDocument.
                    Include(x => x.ProductDocumentType).
                    Where(x => x.ProductId == productId).
                    Select(x => new ProductDocumentGetDTO
                    {
                        ProductId = x.ProductId,
                        ProductDocumentId = x.Id,
                        DocName = x.Name,
                        DocPath = x.DocPath,
                        TypeId = x.ProductDocumentType.Id,
                        TypeName = x.ProductDocumentType.Name,
                    }).
                    OrderBy(x => x.TypeId).ToListAsync();

            if (productDoctos == null)
                return NotFound("No results found for Product Id: " + productId);

            return productDoctos;
        }

        /// <summary>
        /// Relaciona un documento con un producto
        /// </summary>
        /// <param name="productDoctoDTO">Modelo</param>
        /// <returns></returns>
        [HttpPost("AddProductDocument")]
        public async Task<ActionResult> AddProductDocument([FromBody] ProductDocumentAddDTO productDoctoDTO)
        {
            var productDocto = mapper.Map<ProductDocument>(productDoctoDTO);

            await context.ProductDocument.AddAsync(productDocto);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Relaciona un documento con un producto de forma masiva
        /// </summary>
        /// <param name="productDoctoDTO">Lista del modelo</param>
        /// <returns></returns>
        [HttpPost("AddProductDocumentBulk")]
        public async Task<ActionResult> AddProductDocumentBulk([FromBody] List<ProductDocumentAddDTO> productDoctoDTO)
        {
            var productDocto = mapper.Map<List<ProductDocument>>(productDoctoDTO);

            await context.ProductDocument.AddRangeAsync(productDocto);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Actualizar Nombre o Ruta del documento relacionado al producto
        /// </summary>
        /// <param name="option">Indicador de campo a actualizar (n = Nombre, p = Ruta)</param>
        /// <param name="productDoctoDTO">Modelo a enviar</param>
        /// <returns></returns>
        [HttpPut("UpdateProductDocument/{option}")]
        public async Task<ActionResult> UpdateProductDocument(string option, [FromBody] ProductDocumentUpdDTO productDoctoDTO)
        {
            if (option != "n" && option != "p")
            {
                ModelState.AddModelError(string.Empty, "Invalid option, value must be 'n' for name or 'p' for path.");
                return (ActionResult)apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
            }

            if (option.Equals("n") && productDoctoDTO.NewValue.Length > 30)
            {
                ModelState.AddModelError(string.Empty, "Name length can't be more than 30.");
                return (ActionResult)apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
            }

            var productDocto = await context.ProductDocument.
                FirstOrDefaultAsync(x => x.ProductId == productDoctoDTO.ProductId && x.Name.Equals(productDoctoDTO.Name));

            if (productDocto is null)
                return NotFound("Product Id " + productDoctoDTO.ProductId + " and Name '" + productDoctoDTO.Name + "' not found.");

            if (option.Equals("p"))
                productDocto.DocPath = productDoctoDTO.NewValue;
            else if(option.Equals("n"))
                productDocto.Name = productDoctoDTO.NewValue;

            productDocto.UpdateDate = DateTime.Now;
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Actualizar de forma masiva el Nombre o Ruta del documento relacionado al producto
        /// </summary>
        /// <param name="option">Indicador de campo a actualizar (n = Nombre, p = Ruta)</param>
        /// <param name="productsDoctoDTO">Lista del modelo</param>
        /// <returns></returns>
        [HttpPut("UpdateProductDocumentBulk/{option}")]
        public async Task<ActionResult> UpdateProductDocumentBulk(string option, [FromBody] List<ProductDocumentUpdDTO> productsDoctoDTO)
        {
            if (option != "n" && option != "p")
            {
                ModelState.AddModelError(string.Empty, "Invalid option, value must be 'n' for name or 'p' for path.");
                return (ActionResult)apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
            }

            if (option.Equals("n"))
                if (productsDoctoDTO.Where(x => x.NewValue.Length > 30).Count() > 0)
                {
                    ModelState.AddModelError(string.Empty, "One or more element in the list have more than 30 characters for Name field.");
                    return (ActionResult)apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
                }

            List<ProductDocument> productsDocto;

            productsDocto = context.ProductDocument.ToList().
                Where(x => productsDoctoDTO.Any(y => x.ProductId.Equals(y.ProductId) && x.Name.Equals(y.Name))).ToList();

            if (productsDocto is null)
                return NotFound("No results found.");

            if (option.Equals("p"))
                productsDocto.ForEach(i =>
                    productsDoctoDTO.Where(y => y.ProductId == i.ProductId && y.Name == i.Name).
                    ToList().ForEach(x => { i.DocPath = x.NewValue; i.UpdateDate = DateTime.Now; }));
            
            else if (option.Equals("n"))
                productsDocto.ForEach(i =>
                    productsDoctoDTO.Where(y => y.ProductId == i.ProductId && y.Name == i.Name).
                    ToList().ForEach(x => { i.Name = x.NewValue; i.UpdateDate = DateTime.Now; }));
            
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Elimina la relación entre un docto y un producto
        /// Nota: si no se envia el docname elimina el primier registro que encuentre
        /// </summary>
        /// <param name="productId">Id del producto</param>
        /// <param name="docName">Nombre del documento</param>
        /// <returns></returns>
        [HttpDelete("DeleteProductDocument/{productId}/{docName?}")]
        public async Task<ActionResult> DeleteProductDocument(int productId, string docName)
        {
            ProductDocument productDocto;

            if (docName is null)
                productDocto = await context.ProductDocument.FirstOrDefaultAsync(x => x.ProductId == productId);
            else
                productDocto = await context.ProductDocument.FirstOrDefaultAsync(x => x.ProductId == productId && x.Name.Equals(docName));

            if (productDocto is null)
                return NotFound();

            context.Remove(productDocto);
            await context.SaveChangesAsync();

            return Ok();
        }

        #endregion
    }
}
