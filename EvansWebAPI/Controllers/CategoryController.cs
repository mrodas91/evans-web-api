﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using EvansWebAPI.Common;
using EvansWebAPI.Contexts;
using EvansWebAPI.Entities;
using EvansWebAPI.EntityLanguages;
using EvansWebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Drawing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;



// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EvansWebAPI.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CategoryController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;
        private readonly IOptions<ApiBehaviorOptions> apiBehaviorOptions;
        private readonly CommonCategory commonCategory;
        private readonly CommonMethods commonMethods;

        public CategoryController(ApplicationDbContext context, IMapper mapper, IConfiguration configuration,
            IOptions<ApiBehaviorOptions> apiBehaviorOptions, CommonMethods commonMethods, CommonCategory commonCategory)
        {
            this.context = context;
            this.mapper = mapper;
            this.configuration = configuration;
            this.apiBehaviorOptions = apiBehaviorOptions;
            this.commonMethods = commonMethods;
            this.commonCategory = commonCategory;
        }

        /// <summary>
        /// Búsqueda de MegaFamilias, Familias y Categorias por Id
        /// </summary>
        /// <param name="id">Id de categoria a buscar</param>
        /// <returns></returns>
        [HttpGet("GetCategoryById/{id}", Name = "GetCategoryById")]
        public async Task<ActionResult<CategoryDTO>> GetCategoryById(int id)
        {
            var category = await context.Category.FirstOrDefaultAsync(x => x.Id == id);

            if (category == null)
                return NotFound();

            var categoryDTO = mapper.Map<CategoryDTO>(category);

            return categoryDTO;
        }

        /// <summary>
        /// Búsqueda de MegaFamilias por nombre
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [HttpGet("GetMegaFamilias/{nombre?}")]
        public async Task<ActionResult<List<CategoryDTO>>> GetMegaFamilia(string nombre)
        {
            List<Category> megaFamilias;

            if (nombre is null)
                megaFamilias = await context.Category.
                Where(x => x.ParentCategoryId.Equals(0)).
                ToListAsync();
            else
                megaFamilias = await context.Category.
                Where(x => x.ParentCategoryId.Equals(0) && x.Name.Contains(nombre)).
                ToListAsync();

            if (megaFamilias is null)
                return NotFound();

            var megaFamiliasDTO = mapper.Map<List<CategoryDTO>>(megaFamilias);

            return megaFamiliasDTO;
        }

        /// <summary>
        /// Búsqueda de Familias por nombre
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [HttpGet("GetFamilias/{nombre?}")]
        public async Task<ActionResult<List<CategoryDTO>>> GetFamilias(string nombre)
        {
            var megaFamilias = await context.Category.
                Where(x => x.ParentCategoryId.Equals(0)).
                Select(x => x.Id).
                ToListAsync();

            if (megaFamilias is null)
                return NotFound();

            List<Category> familias;

            if(nombre is null)
                familias = await context.Category.
                    Where(x => megaFamilias.Contains(x.ParentCategoryId)).ToListAsync();
            else
                familias = await context.Category.
                    Where(x => megaFamilias.Contains(x.ParentCategoryId) && x.Name.Contains(nombre)).ToListAsync();


            var familiasDTO = mapper.Map<List<CategoryDTO>>(familias);

            return familiasDTO;
        }

        /// <summary>
        /// Búsqueda de Categorias por nombre
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [HttpGet("GetCategorias/{nombre?}")]
        public async Task<ActionResult<List<CategoryDTO>>> GetCategorias(string nombre)
        {
            var megaFamilias = await context.Category.
                Where(x => x.ParentCategoryId.Equals(0)).
                Select(x => x.Id).ToListAsync();

            if (megaFamilias is null)
                return NotFound();

            var familias = await context.Category.
                Where(x => megaFamilias.Contains(x.ParentCategoryId)).
                Select(x => x.Id).ToListAsync();

            if (familias is null)
                return NotFound();

            List<Category> categorias;

            if(nombre is null)
                categorias = await context.Category.
                    Where(x => familias.Contains(x.ParentCategoryId)).ToListAsync();
            else
                categorias = await context.Category.
                    Where(x => familias.Contains(x.ParentCategoryId) && x.Name.Contains(nombre)).ToListAsync();

            var categoriasDTO = mapper.Map<List<CategoryDTO>>(categorias);

            return categoriasDTO;
        }

        /// <summary>
        /// Búsqueda de una MegaFamilia, Familia o Categoria por nombre mostrando las ramas dependientes
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [HttpGet("GetArbolCategorias/{nombre?}")]
        public async Task<ActionResult<List<CategoryTreeDTO>>> GetArbolCategoria(string nombre)
        {
            return await commonCategory.GetAllByName(nombre);
        }

        /// <summary>
        /// Crea una nueva MegaFamilia, Familia o Categoria
        /// </summary>
        /// <param name="categoryDTO"></param>
        /// <returns></returns>
        [HttpPost("AddCategory")]
        public async Task<ActionResult> AddCategory([FromBody] CategoryAddDTO categoryDTO)
        {
            var category = mapper.Map<Category>(categoryDTO);

            category.CreatedOnUtc = DateTime.UtcNow;
            category.UpdatedOnUtc = category.CreatedOnUtc;

            if (category.PageSize.Equals(0))
                category.PageSize = 6;
            if (string.IsNullOrWhiteSpace(category.PageSizeOptions))
                category.PageSizeOptions = configuration["DefaultDbValues:PageSizeOptions"];
            category.AllowCustomersToSelectPageSize = true;
            category.CategoryTemplateId = 1;
            category.IsCollection = false;

            context.Category.Add(category);
            await context.SaveChangesAsync();

            if (category.Id != 0)
            {
                #region URL Settings
                bool result = await commonMethods.CreateUrlRecord("Category", category.Id, category.Name, Convert.ToInt32(configuration["DefaultDbValues:SpanishLanguageId"]));
                #endregion

                #region Language Settings
                if (category.CategoryLanguageValues is not null)
                    await commonMethods.AddEnglishValues(category.CategoryLanguageValues, "Category", category.Id);
                #endregion

                #region Adding Picture
                if(categoryDTO.PicturePath is not null)
                    await commonCategory.AddCategoryPicture(categoryDTO.PicturePath, category);
                #endregion
            }

            var categoryCreatedDTO = mapper.Map<CategoryDTO>(category);

            return new CreatedAtRouteResult("GetCategoryById", new { id = category.Id }, categoryCreatedDTO);
        }

        [HttpPut("UpdateCategory")]
        public async Task<ActionResult> UpdateCategory([FromBody] CategoryUpdateDTO categoryDTO)
        {
            Category category = await context.Category.FirstOrDefaultAsync(x => x.Id == categoryDTO.CategoryId);

            if (category is null)
                return NotFound();

            if (categoryDTO.Name is not null) category.Name = categoryDTO.Name;
            if (categoryDTO.Description is not null) category.Description = categoryDTO.Description;
            if (categoryDTO.ParentCategoryId is not null) category.ParentCategoryId = (int)categoryDTO.ParentCategoryId;
            if (categoryDTO.PictureId is not null) category.PictureId = (int)categoryDTO.PictureId;
            if (categoryDTO.PageSize is not null) category.PageSize = (int)categoryDTO.PageSize;
            if (categoryDTO.PageSizeOptions is not null) category.PageSizeOptions = categoryDTO.PageSizeOptions;
            if (categoryDTO.ShowOnHomepage is not null) category.ShowOnHomepage = (bool)categoryDTO.ShowOnHomepage;
            if (categoryDTO.IncludeInTopMenu is not null) category.IncludeInTopMenu = (bool)categoryDTO.IncludeInTopMenu;
            if (categoryDTO.DisplayOrder is not null) category.DisplayOrder = (int)categoryDTO.DisplayOrder;

            category.UpdatedOnUtc = DateTime.UtcNow;

            await context.SaveChangesAsync();

            if (categoryDTO.Name is not null)
                await commonMethods.UpdateUrlRecord("Category", category.Id, category.Name,
                        Convert.ToInt32(configuration["DefaultDbValues:SpanishLanguageId"]));

            if (categoryDTO.CategoryLanguageValues is not null)
            {
                DefaultLanguageValues languageValues = mapper.Map<DefaultLanguageValues>(categoryDTO.CategoryLanguageValues);
                await commonMethods.AddEnglishValues(languageValues, "Category", category.Id);
            }

            return Ok();
        }

        /// <summary>
        /// Borrado logico, desactiva una categoria
        /// </summary>
        /// <param name="id">Id Categoria a eliminar</param>
        /// <returns></returns>
        [HttpPut("EnableDisableCategory/{categoryId}")]
        public async Task<ActionResult> EnableDisableCategory(int categoryId)
        {
            Category category = await context.Category.FirstOrDefaultAsync(x => x.Id == categoryId);

            if (category is null)
                return NotFound();

            category.Published = !category.Published;
            category.ShowOnHomepage = false;
            category.UpdatedOnUtc = DateTime.UtcNow;

            await context.SaveChangesAsync();

            await commonMethods.EnableDisableUrlRecords("Category", categoryId);

            string result = category.Published == true ? "Enable" : "Disable";

            return Ok(result);
        }

        /// <summary>
        /// Elimina una Categoría en base a su Id
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpDelete("RemoveCategory/{categoryId}")]
        public async Task<ActionResult> RemoveCategory(int categoryId)
        {
            if (!commonCategory.CategoryExist(categoryId))
                return BadRequest("The CategoryId does not exist.");

            List<Category> categoriesRelated = context.Category.Where(x => x.ParentCategoryId == categoryId).ToList();
            if (categoriesRelated.Count() > 0)
                return BadRequest("The Category can't be removed becuase it has " + categoriesRelated.Count + " Categories related.");

            List<ProductCategoryMapping> categoryProductList = await context.ProductCategoryMapping.Where(x => x.CategoryId == categoryId).ToListAsync();
            if (categoryProductList.Count > 0)
                return BadRequest("The Category can't be remove becuase it has " + categoryProductList.Count + " Product(s) related.");

            Category categoryToRemove = await context.Category.FirstOrDefaultAsync(x => x.Id == categoryId);
            context.Category.Remove(categoryToRemove);
            await context.SaveChangesAsync();

            await commonMethods.DeleteUrlRecords("Category", categoryToRemove.Id);
            await commonMethods.DeleteEnglishValues("Category", categoryToRemove.Id);

            return Ok();
        }

        /// <summary>
        /// Agrega los valores en inglés para Categorías
        /// </summary>
        /// <param name="languageValuesDTO">Model</param>
        /// <returns></returns>
        [HttpPut("AddCategoryLangageValues")]
        public async Task<ActionResult> AddCategoryLangageValues([FromBody] List<CategoryLanguageValuesDTO> languageValuesDTO)
        {
            if (languageValuesDTO.Where(x => x.EntityId == null).ToList().Count() > 0)
                return BadRequest("EntityId is required for all of the elements in the list.");
            if (languageValuesDTO.Where(x => commonCategory.CategoryExist((int)x.EntityId) == true).ToList().Count() < languageValuesDTO.Count())
                return BadRequest("Not all of the Categories in the model exist.");

            List<DefaultLanguageValues> languageValuesList = mapper.Map<List<DefaultLanguageValues>>(languageValuesDTO);
            await commonMethods.AddEnglishValues(languageValuesList, "Category");

            return Ok();
        }

        /// <summary>
        /// Agrega agrega una imágen a la Categoría
        /// </summary>
        /// <param name="categoryPictureAddDTO">Model</param>
        /// <returns></returns>
        [HttpPost("AddCategoryPicture")]
        public async Task<ActionResult> AddCategoryPicture([FromBody] CategoryPictureAddDTO categoryPictureAddDTO)
        {
            Category category = await context.Category.FirstOrDefaultAsync(x => x.Id == categoryPictureAddDTO.CategoryId);

            if (category is null)
                return NotFound("Category not found");

            await commonCategory.AddCategoryPicture(categoryPictureAddDTO.LocalPath, category);

            return Ok();
        }

        /// <summary>
        /// Agrega un Banner a una cagegoría Familia.
        /// </summary>
        /// <param name="categoryBannerDTO">Model</param>
        /// <returns></returns>
        [HttpPost("AddCategoryBanner")]
        public async Task<ActionResult> AddCategoryBanner([FromBody] CategoryBannerAddDTO categoryBannerDTO)
        {
            Category category = await context.Category.FirstOrDefaultAsync(x => x.Id == categoryBannerDTO.CategoryId);

            if (category is null)
                return NotFound("Category not found");

            if (!commonCategory.IsFamilia(categoryBannerDTO.CategoryId))
                return BadRequest("Banners can only be related to Family categories.");
            if (commonCategory.IsCollection(categoryBannerDTO.CategoryId))
                return BadRequest("Banners can't be added to Collections.");
            if (commonCategory.IsPromotion(categoryBannerDTO.CategoryId))
                return BadRequest("Banners can't be added to Promotions.");

            category.BannerURL = categoryBannerDTO.BannerURL;
            category.IsVideo = categoryBannerDTO.IsVideo;

            await context.SaveChangesAsync();

            return Ok();
        }
    }
}
