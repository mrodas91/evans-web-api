﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EvansWebAPI.Common;
using EvansWebAPI.Contexts;
using EvansWebAPI.Entities;
using EvansWebAPI.EntityLanguages;
using EvansWebAPI.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EvansWebAPI.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PromotionController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;
        private readonly CommonCategory commonCategory;
        private readonly CommonMethods commonMethods;

        public PromotionController(ApplicationDbContext context, IMapper mapper, IConfiguration configuration,
            CommonMethods commonMethods, CommonCategory commonCategory)
        {
            this.context = context;
            this.mapper = mapper;
            this.configuration = configuration;
            this.commonMethods = commonMethods;
            this.commonCategory = commonCategory;
        }

        /// <summary>
        /// Retorna todas las promociones
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAll")]
        public async Task<ActionResult<List<PromotionDTO>>> GetAll()
        {
            List<Category> promotionList = await context.Category.
                Where(x => x.ParentCategoryId == Convert.ToInt32(configuration["DefaultDbValues:PromotionCategoryId"])).ToListAsync();

            if (promotionList == null)
                return NotFound();

            List<PromotionDTO> promotionDTO = mapper.Map<List<PromotionDTO>>(promotionList);

            return promotionDTO;
        }

        /// <summary>
        /// Retorna una Promoción por su Id
        /// </summary>
        /// <param name="id">Id de Promoción</param>
        /// <returns></returns>
        [HttpGet("GetPromotionById/{id}", Name = "GetPromotionById")]
        public async Task<ActionResult<PromotionDTO>> GetPromotionById(int id)
        {
            if (!commonCategory.IsPromotion(id))
                return BadRequest("This CategoryId does not belong to a Promotion.");

            Category promotion = await context.Category.FirstOrDefaultAsync(x => x.Id == id);

            if (promotion == null)
                return NotFound();

            PromotionDTO promotionDTO = mapper.Map<PromotionDTO>(promotion);

            return promotionDTO;
        }

        /// <summary>
        /// Retorna una Promoción por su Nombre
        /// </summary>
        /// <param name="name">Nombre de la Promoción</param>
        /// <returns></returns>
        [HttpGet("GetPromotionByName/{name}")]
        public async Task<ActionResult<PromotionDTO>> GetPromotionByName(string name)
        {
            Category promotion = await context.Category.
                FirstOrDefaultAsync(x => x.Name.Contains(name) && x.ParentCategoryId == Convert.ToInt32(configuration["DefaultDbValues:PromotionCategoryId"]));

            if (promotion == null)
                return NotFound();

            PromotionDTO promotionDTO = mapper.Map<PromotionDTO>(promotion);

            return promotionDTO;
        }

        /// <summary>
        /// Retorna el arbol de Promociones
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetArbolPromociones")]
        public async Task<ActionResult<List<CategoryTreeDTO>>> GetArbolPromociones()
        {
            return await commonCategory.GetAllByName("Promociones");
        }

        /// <summary>
        /// Crea una Promoción nueva.
        /// Las Promociones son categorias dentro de la base de datos, estas se asignan directamente a la MegaFamilia "Promociones".
        /// </summary>
        /// <param name="promotionDTO">Modelo</param>
        /// <returns></returns>
        [HttpPost("CreatePromotion")]
        public async Task<ActionResult> CreatePromotionSebCategroy([FromBody] PromotionAddDTO promotionDTO)
        {
            Category promotion = mapper.Map<Category>(promotionDTO);

            promotion.ParentCategoryId = Convert.ToInt32(configuration["DefaultDbValues:PromotionCategoryId"]);
            promotion.IsCollection = false;

            if (promotion.PageSize.Equals(0))
                promotion.PageSize = Convert.ToInt32(configuration["DefaultDbValues:PageSize"]);
            if (string.IsNullOrWhiteSpace(promotion.PageSizeOptions))
                promotion.PageSizeOptions = configuration["DefaultDbValues:PageSizeOptions"];

            promotion.AllowCustomersToSelectPageSize = true;
            promotion.CategoryTemplateId = 1;
            promotion.CreatedOnUtc = DateTime.UtcNow;
            promotion.UpdatedOnUtc = promotion.CreatedOnUtc;

            context.Category.Add(promotion);
            await context.SaveChangesAsync();

            if (promotion.Id != 0)
            {
                #region URL Settings
                bool result = await commonMethods.CreateUrlRecord("Category", promotion.Id, promotion.Name, Convert.ToInt32(configuration["DefaultDbValues:SpanishLanguageId"]));
                #endregion

                #region Language Settings
                if (promotion.CategoryLanguageValues is not null)
                    await commonMethods.AddEnglishValues(promotion.CategoryLanguageValues, "Category", promotion.Id);
                #endregion

                #region Adding Picture
                if (promotionDTO.PicturePath is not null)
                    await commonCategory.AddCategoryPicture(promotionDTO.PicturePath, promotion);
                #endregion
            }

            PromotionDTO promotionCreatedDTO = mapper.Map<PromotionDTO>(promotion);

            return new CreatedAtRouteResult("GetPromotionById", new { id = promotion.Id }, promotionCreatedDTO);
        }

        /// <summary>
        /// Actualiza los datos de una colección
        /// Las colecciones son categorias dentro de la base de datos, estas se asignan directamente a la MegaFamilia "Promociones".
        /// </summary>
        /// <param name="promotionDTO">Modelo</param>
        /// <returns></returns>
        [HttpPut("UpdatePromotion")]
        public async Task<ActionResult> UpdatePromotionSubCategory([FromBody] PromotionUpdateDTO promotionDTO)
        {
            //if (!commonCategory.IsPromotion(promotionDTO.PromotionId))
            //    return BadRequest("The Id does not belong to a Promotion.");

            Category promotion = await context.Category.FirstOrDefaultAsync(x => x.Id == promotionDTO.PromotionId);

            if (promotion is null)
                return NotFound();

            if (!commonCategory.IsPromotion(promotion.Id))
                return BadRequest("The Id does not belong to a Promotion.");

            if (promotionDTO.Name is not null) promotion.Name = promotionDTO.Name;
            if (promotionDTO.Description is not null) promotion.Description = promotionDTO.Description;
            if (promotionDTO.PageSize is not null) promotion.PageSize = (int)promotionDTO.PageSize;
            if (promotionDTO.PageSizeOptions is not null) promotion.PageSizeOptions = promotionDTO.PageSizeOptions;
            if (promotionDTO.DisplayOrder is not null) promotion.DisplayOrder = (int)promotionDTO.DisplayOrder;

            promotion.UpdatedOnUtc = DateTime.UtcNow;

            await context.SaveChangesAsync();

            #region UrlRecord
            if (promotionDTO.Name is not null)
            {
                await commonMethods.UpdateUrlRecord("Category", promotion.Id, promotion.Name,
                        Convert.ToInt32(configuration["DefaultDbValues:SpanishLanguageId"]));
            }
            #endregion

            #region Language Settings
            if (promotionDTO.CategoryLanguageValues is not null)
            {
                DefaultLanguageValues languageValues = mapper.Map<DefaultLanguageValues>(promotionDTO.CategoryLanguageValues);
                await commonMethods.AddEnglishValues(languageValues, "Category", promotion.Id);
            }
            #endregion

            return Ok();
        }

        /// <summary>
        /// Agrega los valores en inglés para Colecciones
        /// </summary>
        /// <param name="languageValuesDTO"></param>
        /// <returns></returns>
        [HttpPut("AddPromotionLanguageValues")]
        public async Task<ActionResult> AddPromotionLangageValues([FromBody] List<CategoryLanguageValuesDTO> languageValuesDTO)
        {
            if (languageValuesDTO.Where(x => x.EntityId == null).ToList().Count() > 0)
                return BadRequest("EntityId is required for all of the elements in the list.");
            if (languageValuesDTO.Where(x => commonCategory.CategoryExist((int)x.EntityId) == true).ToList().Count() < languageValuesDTO.Count())
                return BadRequest("Not all of the Categories in the model exist.");
            if (languageValuesDTO.Where(x => commonCategory.IsPromotion((int)x.EntityId) == true).ToList().Count() < languageValuesDTO.Count())
                return BadRequest("Not all of the CategoryId belong to a Promotion.");

            List<DefaultLanguageValues> languageValuesList = mapper.Map<List<DefaultLanguageValues>>(languageValuesDTO);
            await commonMethods.AddEnglishValues(languageValuesList, "Category");

            return Ok();
        }

        /// <summary>
        /// Habilita o deshabilita un Promocion
        /// </summary>
        /// <param name="promotionId">Id de Promoción</param>
        /// <returns></returns>
        [HttpPut("EnableDisablePromotion/{promotionId}")]
        public async Task<ActionResult> EnableDisablePromotion(int promotionId)
        {
            Category promotion = await context.Category.FirstOrDefaultAsync(x => x.Id == promotionId);

            if (promotion is null)
                return NotFound();

            promotion.Published = !promotion.Published;
            promotion.UpdatedOnUtc = DateTime.UtcNow;
            await context.SaveChangesAsync();

            await commonMethods.EnableDisableUrlRecords("Category", promotionId);
            string result = promotion.Published == true ? "Enable" : "Disable";

            return Ok(result);
        }

        /// <summary>
        /// Elimina una Promoción
        /// </summary>
        /// <param name="promotionId">Promoción Id</param>
        /// <returns></returns>
        [HttpDelete("RemovePromotion/{promotionId}")]
        public async Task<ActionResult> RemovePromotion(int promotionId)
        {
            if (!commonCategory.CategoryExist(promotionId))
                return BadRequest("The CategoryId of this Promotion does not exist.");
            if (!commonCategory.IsPromotion(promotionId))
                return BadRequest("The CategoryId does not belong to a Promotion.");

            List<ProductCategoryMapping> promotionProductList = await context.ProductCategoryMapping.Where(x => x.CategoryId == promotionId).ToListAsync();
            if (promotionProductList.Count > 0)
                return BadRequest("The Promotion can't be remove becuase it has " + promotionProductList.Count + " Product(s) related.");

            Category promotionToRemove = await context.Category.FirstOrDefaultAsync(x => x.Id == promotionId);
            context.Category.Remove(promotionToRemove);
            await context.SaveChangesAsync();

            await commonMethods.DeleteUrlRecords("Category", promotionToRemove.Id);
            await commonMethods.DeleteEnglishValues("Category", promotionToRemove.Id);

            return Ok();
        }

        /// <summary>
        /// Agrega un producto a una Promoción
        /// </summary>
        /// <param name="PromotionDTO">Modelo</param>
        /// <returns></returns>
        [HttpPost("AddProductToPromotion")]
        public async Task<ActionResult> AddProductToPromotion([FromBody] PromotionCategoryMappingAddDTO PromotionDTO)
        {
            if (!commonCategory.CategoryExist(PromotionDTO.CategoryId))
                return BadRequest("The CategoryId of this Promotion does not exist.");
            if (!commonCategory.IsPromotion(PromotionDTO.CategoryId))
                return BadRequest("The CategoryId does not belong to a Promotion.");
            if (!commonMethods.ProductExits(PromotionDTO.ProductId))
                return BadRequest("The ProductId does not exist.");
            if (context.ProductCategoryMapping.ToList().Where(x => x.CategoryId == PromotionDTO.CategoryId && x.ProductId == PromotionDTO.ProductId).ToList().Count() > 0)
                return BadRequest("The ProductId is already related to this Promotion.");

            ProductCategoryMapping promotionMapping = mapper.Map<ProductCategoryMapping>(PromotionDTO);

            await context.AddAsync(promotionMapping);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Agrega una serie de Productos a una Promoción
        /// </summary>
        /// <param name="PromotionDTO">Lista del Modelo</param>
        /// <returns></returns>
        [HttpPost("AddProductToPromotionBulk")]
        public async Task<ActionResult> AddProductToPromotionBulk([FromBody] List<PromotionCategoryMappingAddDTO> PromotionDTO)
        {
            if (PromotionDTO.Where(x => commonCategory.CategoryExist(x.CategoryId) == true).ToList().Count() < PromotionDTO.Count())
                return BadRequest("Not all of the CategoryIds Promotion exist.");
            if (PromotionDTO.Where(x => commonCategory.IsPromotion(x.CategoryId) == true).ToList().Count() < PromotionDTO.Count())
                return BadRequest("Not all of the CategoryIds belong to a Promotion.");
            if (PromotionDTO.Where(x => commonMethods.ProductExits(x.ProductId) == true).ToList().Count() < PromotionDTO.Count())
                return BadRequest("Not all of the ProductIds exist.");
            if (context.ProductCategoryMapping.ToList().Where(x => PromotionDTO.Any(y => x.CategoryId == y.CategoryId && x.ProductId == y.ProductId)).ToList().Count() > 0)
                return BadRequest("Ore or more ProductIds are already related to this Promotion.");

            List<ProductCategoryMapping> promotionMapping = mapper.Map<List<ProductCategoryMapping>>(PromotionDTO);

            await context.AddRangeAsync(promotionMapping);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Elimina un producto de una Promoción
        /// </summary>
        /// <param name="PromotionDTO">Modelo</param>
        /// <returns></returns>
        [HttpDelete("RemoveProductFromPromotion")]
        public async Task<ActionResult> RemoveProductFromPromotion([FromBody] PromotionCategoryMappingRemoveDTO PromotionDTO)
        {
            if (!commonCategory.CategoryExist(PromotionDTO.CategoryId))
                return BadRequest("The CategoryId of this Promotion does not exist.");
            if (!commonCategory.IsPromotion(PromotionDTO.CategoryId))
                return BadRequest("The CategoryId does not belong to a Promotion.");
            if (!commonMethods.ProductExits(PromotionDTO.ProductId))
                return BadRequest("The ProductId does not exist.");

            ProductCategoryMapping PromotionMappingToRemove = await context.ProductCategoryMapping.
                FirstOrDefaultAsync(x => x.CategoryId == PromotionDTO.CategoryId && x.ProductId == PromotionDTO.ProductId);

            if (PromotionMappingToRemove is null)
                return BadRequest("No values found.");

            context.Remove(PromotionMappingToRemove);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Remueve una serie de productos de una o varias Promociones
        /// </summary>
        /// <param name="PromotionDTO"></param>
        /// <returns></returns>
        [HttpDelete("RemoveProductFromPromotionBulk")]
        public async Task<ActionResult> RemoveProductFromPromotionBulk([FromBody] List<PromotionCategoryMappingRemoveDTO> PromotionDTO)
        {
            if (PromotionDTO.Where(x => commonCategory.CategoryExist(x.CategoryId) == true).ToList().Count() < PromotionDTO.Count())
                return BadRequest("Not all of the CategoryId Promotion exist.");
            if (PromotionDTO.Where(x => commonCategory.IsPromotion(x.CategoryId) == true).ToList().Count() < PromotionDTO.Count())
                return BadRequest("Not all of the CategoryIds belong to a Promotion.");
            if (PromotionDTO.Where(x => commonMethods.ProductExits(x.ProductId) == true).ToList().Count() < PromotionDTO.Count())
                return BadRequest("Not all of the ProductIds exist.");

            List<ProductCategoryMapping> promotionMappingToRemove = context.ProductCategoryMapping.ToList()
                    .Where(x => PromotionDTO.Any(y => x.CategoryId == y.CategoryId && x.ProductId == y.ProductId)).ToList();

            if (promotionMappingToRemove.Count() == 0)
                return NotFound("No values were found.");
            if (promotionMappingToRemove.Count < PromotionDTO.Count())
                return BadRequest("Not all of the relations exist in the db.");

            context.RemoveRange(promotionMappingToRemove);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Remover todos los productos de una coleción
        /// </summary>
        /// <param name="promotionId">Id de Colleción</param>
        /// <returns></returns>
        [HttpDelete("RemoveAllProductsFromPromotion/{promotionId}")]
        public async Task<ActionResult> RemoveAllProductsFromPromotion(int promotionId)
        {
            if (!commonCategory.CategoryExist(promotionId))
                return BadRequest("The CategoryId of this Promotion does not exist.");
            if (!commonCategory.IsPromotion(promotionId))
                return BadRequest("The CategoryId does not belong to a Promotions.");


            List<ProductCategoryMapping> promotionMappingToRemove = await context.ProductCategoryMapping.
                Where(x => x.CategoryId == promotionId).ToListAsync();

            if (promotionMappingToRemove.Count() == 0)
                return BadRequest("No values found.");

            context.RemoveRange(promotionMappingToRemove);
            await context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Agrega una imagen a la Promoción
        /// </summary>
        /// <param name="PromotionPictureAddDTO">Model</param>
        /// <returns></returns>
        [HttpPost("AddPromotionPicture")]
        public async Task<ActionResult> AddPromotionPicture([FromBody] CategoryPictureAddDTO PromotionPictureAddDTO)
        {
            if (!commonCategory.CategoryExist(PromotionPictureAddDTO.CategoryId))
                return BadRequest("The CategoryId of this Promotion does not exist.");
            if (!commonCategory.IsPromotion(PromotionPictureAddDTO.CategoryId))
                return BadRequest("The CategoryId does not belong to a Promotion.");

            Category category = await context.Category.FirstOrDefaultAsync(x => x.Id == PromotionPictureAddDTO.CategoryId);

            if (category is null)
                return NotFound();

            await commonCategory.AddCategoryPicture(PromotionPictureAddDTO.LocalPath, category);

            return Ok();
        }
    }
}
