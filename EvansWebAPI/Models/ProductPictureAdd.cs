﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class ProductPictureAdd
    {
        [Required]
        public string LocalPath { get; set; }

        [Required]
        public int ProductId { get; set; }
    }
}
