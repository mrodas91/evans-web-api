﻿using System;
namespace EvansWebAPI.Models
{
    public class CategoryLanguageValuesDTO
    {
        public string EnName { get; set; }

        public string EnShortDescription { get; set; }

        public int? EntityId { get; set; }
    }
}
