﻿using System;
namespace EvansWebAPI.Models
{
    public class ProductShortInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
    }
}
