﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class ProductSpecificationAttributesRemoveDTO
    {
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int SpecificationAttributeOptionId { get; set; }
        [Required]
        public int ProductDescriptionTypeId { get; set; }
    }
}
