﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class SpecificationAttributeOptionAddDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int SpecificationAttributeId { get; set; }
        public int DisplayOrder { get; set; }
        
        public virtual DefaultLanguageValueDTO DefaultLanguageValues { get; set; }
    }
}
