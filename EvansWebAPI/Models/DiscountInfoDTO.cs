﻿using System;
using System.Collections.Generic;
using EvansWebAPI.Entities;

namespace EvansWebAPI.Models
{
    public class DiscountInfoDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CouponCode { get; set; }

        public string AdminComment { get; set; }

        public int DiscountTypeId { get; set; }

        public string DiscountType { get; set; }

        public decimal DiscountPercentage { get; set; }

        public decimal DiscountAmount { get; set; }

        public decimal? MaximumDiscountAmount { get; set; }

        public DateTime? StartDateUtc { get; set; }

        public DateTime? EndDateUtc { get; set; }

        public bool IsCumulative { get; set; }

        public int DiscountLimitationId { get; set; }

        public string DiscountLimitation { get; set; }

        public int LimitationTimes { get; set; }

        public int? MaximumDiscountedQuantity { get; set; }

        public bool? AppliedToSubCategories { get; set; }


        public virtual ICollection<DiscountAppliedToProductsAddDTO> DiscountAppliedToProducts { get; set; }
        public virtual ICollection<DiscountAppliedToCategoriesAddDTO> DiscountAppliedToCategories { get; set; }
    }
}
