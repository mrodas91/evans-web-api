﻿using System;
namespace EvansWebAPI.Models
{
    public class ProductDocumentGetDTO
    {
        public int ProductId { get; set; }
        public int ProductDocumentId { get; set; }
        public string DocName { get; set; }        
        public string DocPath { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
    }
}
