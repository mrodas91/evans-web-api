﻿using System;
namespace EvansWebAPI.Models
{
    public class ProductDescriptionTypeInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
