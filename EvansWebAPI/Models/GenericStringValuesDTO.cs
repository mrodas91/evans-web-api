﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class GenericLanguajeValueDTO
    {
        [Required]
        public string Value { get; set; }
        //[Required]
        public string EnValue { get; set; }

        public int? EntityId { get; set; }
    }
}
