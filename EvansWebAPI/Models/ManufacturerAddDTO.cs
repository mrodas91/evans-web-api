﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class ManufacturerAddDTO
    {
        [Required]
        public string Name { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaTitle { get; set; }
        public string PriceRanges { get; set; }
        public string PageSizeOptions { get; set; }
        public string Description { get; set; }
        public int ManufacturerTemplateId { get; set; }
        public string MetaDescription { get; set; }
        [Required]
        public int PictureId { get; set; }
        [Required]
        public int PageSize { get; set; }
        public bool AllowCustomersToSelectPageSize { get; set; }
        public bool SubjectToAcl { get; set; }
        [Required]
        public bool LimitedToStores { get; set; }
        [Required]
        public bool Published { get; set; }
        [Required]
        public int DisplayOrder { get; set; }
    }
}
