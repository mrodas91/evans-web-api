﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class PromotionCategoryMappingRemoveDTO
    {
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int ProductId { get; set; }
    }
}
