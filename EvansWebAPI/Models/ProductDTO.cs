﻿using System;
namespace EvansWebAPI.Models
{
    public class ProductInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public decimal Price { get; set; }
        public decimal Weight { get; set; }
        public decimal Length { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public int DisplayOrder { get; set; }
        public bool? MarkAsNew { get; set; }
        public bool ShowOnHomepage { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
    }
}
