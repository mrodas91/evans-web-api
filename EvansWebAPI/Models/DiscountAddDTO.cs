﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EvansWebAPI.Entities;

namespace EvansWebAPI.Models
{
    public class DiscountAddDTO
    {
        [Required]
        public string Name { get; set; }

        public string CouponCode { get; set; }      //Solo si RequiresCuponCode es true

        public string AdminComment { get; set; }

        [Required]
        [Range(1, 4, ErrorMessage = "For DiscountTypeId use 1 = Total orden, 2 = Productos, 3 = Categorías, 4 = Envio")]
        public int DiscountTypeId { get; set; }     //Asignado a 1 = Total orden, 2 = Productos, 5 = Categorías, 10 = Envio

        public decimal DiscountPercentage { get; set; }     //Amount es 0 cuando percentage tiene un valor

        public decimal DiscountAmount { get; set; }         //Discount es 0 cuando amount tiene un valor

        public decimal? MaximumDiscountAmount { get; set; } //0 cuando Amount tiene valor

        public DateTime? StartDateUtc { get; set; }

        public DateTime? EndDateUtc { get; set; }
        
        [Required]
        public bool IsCumulative { get; set; }

        [Required]
        [Range(0, 2, ErrorMessage = "For DiscountLimitationId use 0 = Ilimitado, 1 = Limitado a N veces, 2 = N veces por cliente")]
        public int DiscountLimitationId { get; set; } //0 = Ilimitado, 15 = Limitado a N veces, 25 = N veces por cliente

        [Range(0, 9999999, ErrorMessage = "LimitationTimes must be  than 0")]
        public int LimitationTimes { get; set; } //Solo si Limitation es diferente de 0

        public int? MaximumDiscountedQuantity { get; set; }  //Aplica solo a Productos y categorías

        public bool? AppliedToSubCategories { get; set; } //Aplica solo a categorias



        public virtual ICollection<DiscountAppliedToProducts> DiscountAppliedToProducts { get; set; }
        public virtual ICollection<DiscountAppliedToCategories> DiscountAppliedToCategories { get; set; }
    }
}
