﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class ProductDescriptionValueAddDTO
    {
        [Required]
        public int? ProductId { get; set; }
        [Required]
        public int? ProductDescriptionTypeId { get; set; }
        [Required]
        public int? ProductDescriptionNameId { get; set; }
        [Required]
        [StringLength(500)]
        public string Value { get; set; }
        public int DisplayOrder { get; set; }
        public virtual ICollection<DefaultLanguageValueDTO> DefaultLanguageValues { get; set; }
    }
}
