﻿using System;
namespace EvansWebAPI.Models
{
    public class ManufacturerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PictureId { get; set; }
    }
}
