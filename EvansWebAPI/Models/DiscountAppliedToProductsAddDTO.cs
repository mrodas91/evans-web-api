﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class DiscountAppliedToProductsAddDTO
    {
        [Required]
        public int DiscountId { get; set; }
        [Required]
        public int ProductId { get; set; }
    }
}
