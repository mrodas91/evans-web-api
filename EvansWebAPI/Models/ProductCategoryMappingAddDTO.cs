﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class ProductCategoryMappingAddDTO
    {
        [Required]
        public int CategoryId { get; set; }
        public int ProductId { get; set; }
        //[Required]
        //public bool IsFeaturedProduct { get; set; }
        [Required]
        public int DisplayOrder { get; set; }

        public virtual ProductAddDTO Product { get; set; }
    }
}
