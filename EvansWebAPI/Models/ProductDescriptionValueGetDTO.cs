﻿using System;
namespace EvansWebAPI.Models
{
    public class ProductDescriptionValueGetDTO
    {
        public int ProductId { get; set; }
        public string DescriptionType { get; set; }
        public string DescriptionName { get; set; }
        public string Value { get; set; }
        public int DisplayOrder { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
