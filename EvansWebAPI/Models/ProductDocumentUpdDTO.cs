﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class ProductDocumentUpdDTO
    {
        [Required]
        public int? ProductId { get; set; }
        [Required]
        [StringLength(30)]
        public string Name { get; set; }
        [Required]
        public string NewValue { get; set; }
    }
}
