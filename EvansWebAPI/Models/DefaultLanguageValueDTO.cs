﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class DefaultLanguageValueDTO
    {
        [Required]
        public string EnName { get; set; }

        public int? EntityId { get; set; }
    }
}
