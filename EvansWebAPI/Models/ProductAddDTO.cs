﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EvansWebAPI.Entities;

namespace EvansWebAPI.Models
{
    public class ProductAddDTO
    {
        [Required]
        public string Name { get; set; }
        //public string MetaTitle { get; set; }
        [Required]
        public string Sku { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public List<string> PicturePaths { get; set; }

        public int OrderMinimumQuantity { get; set; }
        public int OrderMaximumQuantity { get; set; }

        [Required]
        [Range(1, 99999999, ErrorMessage = "Price can't be 0")]
        public decimal? Price { get; set; }
        [Range(0, 99999999, ErrorMessage = "Price must be between 0 and 99999999")]
        public decimal OldPrice { get; set; }
        [Required]
        [Range(0.0001, 99999999, ErrorMessage = "Weight can't be 0")]
        public decimal? Weight { get; set; }
        [Required]
        [Range(0.0001, 99999999, ErrorMessage = "Length can't be 0")]
        public decimal? Length { get; set; }
        [Required]
        [Range(0.0001, 99999999, ErrorMessage = "Width can't be 0")]
        public decimal? Width { get; set; }
        [Required]
        [Range(0.0001, 99999999, ErrorMessage = "Height can't be 0")]
        public decimal? Height { get; set; }
        public bool IsTaxExempt { get; set; }

        public DateTime? AvailableStartDateTimeUtc { get; set; }
        public DateTime? AvailableEndDateTimeUtc { get; set; }
        public int DisplayOrder { get; set; }
        [Required]
        public bool? Published { get; set; }
        [Required]
        public bool? MarkAsNew { get; set; }
        public DateTime? MarkAsNewStartDateTimeUtc { get; set; }
        public DateTime? MarkAsNewEndDateTimeUtc { get; set; }

        public virtual ICollection<ProductCategoryMappingAddDTO> ProductCategoryMapping { get; set; }
        public virtual ProductLanguageValuesDTO ProductLanguageValues { get; set; }

        #region Commented Section
        //public int ManageInventoryMethodId { get; set; }
        //public bool DisableBuyButton { get; set; }
        //public bool DisableWishlistButton { get; set; }
        //[Required]
        //public int? MinStockQuantity { get; set; }

        //public string MetaKeywords { get; set; }
        //public string MetaTitle { get; set; }
        //public string ManufacturerPartNumber { get; set; }
        //public string Gtin { get; set; }
        //public string RequiredProductIds { get; set; }
        //public string AllowedQuantities { get; set; }
        //[Required]
        //public int ProductTypeId { get; set; }
        //[Required]
        //public int ParentGroupedProductId { get; set; }
        //public bool VisibleIndividually { get; set; }
        //public string AdminComment { get; set; }
        //[Required]
        //public int ProductTemplateId { get; set; }
        //[Required]
        //public int VendorId { get; set; }
        //public bool ShowOnHomepage { get; set; }
        //public string MetaDescription { get; set; }
        //public bool AllowCustomerReviews { get; set; }
        //public int ApprovedRatingSum { get; set; }
        //public int NotApprovedRatingSum { get; set; }
        //public int ApprovedTotalReviews { get; set; }
        //public int NotApprovedTotalReviews { get; set; }
        //public bool SubjectToAcl { get; set; }
        //public bool LimitedToStores { get; set; }
        //public bool IsGiftCard { get; set; }
        //public int GiftCardTypeId { get; set; }
        //public decimal? OverriddenGiftCardAmount { get; set; }
        //public bool RequireOtherProducts { get; set; }
        //public bool AutomaticallyAddRequiredProducts { get; set; }
        //public bool IsDownload { get; set; }
        //public int DownloadId { get; set; }
        //public bool UnlimitedDownloads { get; set; }
        //public int MaxNumberOfDownloads { get; set; }
        //public int? DownloadExpirationDays { get; set; }
        //public int DownloadActivationTypeId { get; set; }
        //public bool HasSampleDownload { get; set; }
        //public int SampleDownloadId { get; set; }
        //public bool HasUserAgreement { get; set; }
        //public string UserAgreementText { get; set; }
        //public bool IsRecurring { get; set; }
        //public int RecurringCycleLength { get; set; }
        //public int RecurringCyclePeriodId { get; set; }
        //public int RecurringTotalCycles { get; set; }
        //public bool IsRental { get; set; }
        //public int RentalPriceLength { get; set; }
        //public int RentalPricePeriodId { get; set; }
        //public bool IsShipEnabled { get; set; }
        //public bool IsFreeShipping { get; set; }
        //public bool ShipSeparately { get; set; }
        //public decimal AdditionalShippingCharge { get; set; }
        //public int DeliveryDateId { get; set; }
        //public bool IsTaxExempt { get; set; }
        //public int TaxCategoryId { get; set; }
        //public bool IsTelecommunicationsOrBroadcastingOrElectronicServices { get; set; }

        //public int ProductAvailabilityRangeId { get; set; }
        //public bool UseMultipleWarehouses { get; set; }
        //[Required]
        //public int WarehouseId { get; set; }
        //[Required]
        //public int StockQuantity { get; set; }
        //public bool DisplayStockAvailability { get; set; }
        //public bool DisplayStockQuantity { get; set; }
        //public int LowStockActivityId { get; set; }
        //public int NotifyAdminForQuantityBelow { get; set; }
        //public int BackorderModeId { get; set; }
        //public bool AllowBackInStockSubscriptions { get; set; }
        //public bool AllowAddingOnlyExistingAttributeCombinations { get; set; }
        //public bool NotReturnable { get; set; }

        //public bool AvailableForPreOrder { get; set; }
        //public DateTime? PreOrderAvailabilityStartDateTimeUtc { get; set; }
        //public bool CallForPrice { get; set; }
        //[Required]
        //public decimal ProductCost { get; set; }
        //public bool CustomerEntersPrice { get; set; }
        //public decimal MinimumCustomerEnteredPrice { get; set; }
        //public decimal MaximumCustomerEnteredPrice { get; set; }
        //public bool BasepriceEnabled { get; set; }
        //public decimal BasepriceAmount { get; set; }
        //public int BasepriceUnitId { get; set; }
        //public decimal BasepriceBaseAmount { get; set; }
        //public int BasepriceBaseUnitId { get; set; }
        #endregion
    }
}
