﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace EvansWebAPI.Models
{
    public class CategoryAddDTO
    {
        [Required]
        public string Name { get; set; }        
        public string Description { get; set; }
        [Required]
        [Range(0, 999999, ErrorMessage = "Parent category can't be negative")]
        public int? ParentCategoryId { get; set; }
        public string PicturePath { get; set; }
        [Required]
        [Range(1, 100, ErrorMessage = "Price must be between 1 and 100")]
        public int? PageSize { get; set; }
        public string PageSizeOptions { get; set; }
        [Required]
        public bool? ShowOnHomepage { get; set; }
        [Required]
        public bool? IncludeInTopMenu { get; set; }
        [Required]
        public bool? Published { get; set; }
        [Required]
        [Range(1, 999999, ErrorMessage = "Display order can't be 0")]
        public int? DisplayOrder { get; set; }
        public virtual CategoryLanguageValuesDTO CategoryLanguageValues { get; set; }
    }
}
