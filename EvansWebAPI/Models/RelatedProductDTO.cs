﻿using System;
namespace EvansWebAPI.Models
{
    public class RelatedProductDTO
    {
        public int ProductOriginId { get; set; }
        public int ProductToRelateId { get; set; }
    }
}
