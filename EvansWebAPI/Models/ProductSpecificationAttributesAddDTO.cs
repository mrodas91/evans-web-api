﻿using System;
using System.ComponentModel.DataAnnotations;
using EvansWebAPI.EntityLanguages;

namespace EvansWebAPI.Models
{
    public class ProductSpecificationAttributesAddDTO
    {
        //public string CustomValue { get; set; }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int SpecificationAttributeOptionId { get; set; }
        //public int AttributeTypeId { get; set; }
        //public bool AllowFiltering { get; set; }
        //public bool ShowOnProductPage { get; set; }
        public int DisplayOrder { get; set; }
        [Required]
        public int ProductDescriptionTypeId { get; set; }
    }
}
