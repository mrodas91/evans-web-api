﻿using System;
namespace EvansWebAPI.Models
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ParentCategoryId { get; set; }
        public bool Published { get; set; }
        public bool ShowOnHomepage { get; set; }
        public int DisplayOrder { get; set; }
    }
}
