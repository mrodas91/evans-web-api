﻿using System;
namespace EvansWebAPI.Models
{
    public class CategoryTreeDTO
    {
        public int? MegaFamiliaId { get; set; }
        public string MegaFamiliaNombre { get; set; }
        public int? MegaFamiliaParent { get; set; }
        public int? FamiliaId { get; set; }
        public string FamiliaNombre { get; set; }
        public int? FamiliaParent { get; set; }
        public int? CategoriaId { get; set; }
        public string CategoriaNombre { get; set; }
        public int? CategoriaParent { get; set; }
    }
}
