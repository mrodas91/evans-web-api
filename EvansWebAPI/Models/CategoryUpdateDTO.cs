﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class CategoryUpdateDTO
    {
        [Required]
        public int CategoryId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [Range(0, 999999, ErrorMessage = "Parent category can't be negative")]
        public int? ParentCategoryId { get; set; }

        public int? PictureId { get; set; }

        [Range(1, 100, ErrorMessage = "PageSize must be between 1 and 100")]
        public int? PageSize { get; set; }

        public string PageSizeOptions { get; set; }

        public bool? ShowOnHomepage { get; set; }

        public bool? IncludeInTopMenu { get; set; }

        //public bool? Published { get; set; }

        [Range(1, 999999, ErrorMessage = "Display order can't be 0")]
        public int? DisplayOrder { get; set; }


        public virtual CategoryLanguageValuesDTO CategoryLanguageValues { get; set; }
    }
}