﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class CategoryBannerAddDTO
    {
        [Required]
        [Url]
        public string BannerURL { get; set; }

        [Required]
        public bool IsVideo { get; set; }

        [Required]
        public int CategoryId { get; set; }
    }
}
