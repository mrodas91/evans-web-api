﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class PromotionUpdateDTO
    {
        [Required]
        public int PromotionId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [Range(1, 100, ErrorMessage = "PageSize must be between 1 and 100")]
        public int? PageSize { get; set; }

        public string PageSizeOptions { get; set; }

        [Range(1, 999999, ErrorMessage = "Display order can't be 0")]
        public int? DisplayOrder { get; set; }

        public virtual CategoryLanguageValuesDTO CategoryLanguageValues { get; set; }
    }
}
