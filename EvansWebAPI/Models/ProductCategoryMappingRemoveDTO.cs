﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class ProductCategoryMappingRemoveDTO
    {
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int ProductId { get; set; }
    }
}
