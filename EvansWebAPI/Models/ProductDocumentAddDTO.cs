﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class ProductDocumentAddDTO
    {
        [Required]
        public int? ProductId { get; set; }
        [Required]
        [StringLength(30)]
        public string Name { get; set; }
        [Required]
        public string DocPath { get; set; }
        [Required]
        public int? ProductDocumentTypeId { get; set; }
    }
}
