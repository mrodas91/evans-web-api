﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class PromotionCategoryMappingAddDTO
    {
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int DisplayOrder { get; set; }
    }
}
