﻿using System;
namespace EvansWebAPI.Models
{
    public class DiscountAppliedToCategoriesAddDTO
    {
        public int DiscountId { get; set; }
        public int CategoryId { get; set; }
    }
}
