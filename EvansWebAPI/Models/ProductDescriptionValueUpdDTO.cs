﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class ProductDescriptionValueUpdDTO
    {
        [Required]
        public int? ProductId { get; set; }
        [Required]
        public int? ProductDescriptionTypeId { get; set; }
        [Required]
        public int? ProductDescriptionNameId { get; set; }
        [StringLength(500)]
        public string Value { get; set; }
        public int? DisplayOrder { get; set; }
    }
}
