﻿using System;
using EvansWebAPI.Entities;

namespace EvansWebAPI.Models
{
    public class SpecificationAttributeOptionInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }

        public virtual SpecificationAttribute SpecificationAttribute { get; set; }
    }
}
