﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class CollectionAddDTO
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string PicturePath { get; set; }
        [Range(1, 100, ErrorMessage = "Price must be between 1 and 100")]
        public int? PageSize { get; set; }
        public string PageSizeOptions { get; set; }
        [Required]
        public bool? Published { get; set; }
        [Required]
        [Range(1, 999999, ErrorMessage = "Display order can't be 0")]
        public int? DisplayOrder { get; set; }
        public virtual CategoryLanguageValuesDTO CategoryLanguageValues { get; set; }
    }
}
