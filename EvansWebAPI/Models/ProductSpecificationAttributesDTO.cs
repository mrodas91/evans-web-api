﻿using System;
using EvansWebAPI.Entities;

namespace EvansWebAPI.Models
{
    public class ProductSpecificationAttributesDTO
    {
        public int Id { get; set; }
        //public int ProductId { get; set; }
        //public int SpecificationAttributeOptionId { get; set; }
        public int DisplayOrder { get; set; }
        public int ProductDescriptionTypeId { get; set; }

        public virtual ProductShortInfoDTO Product { get; set; }
        public virtual SpecificationAttributeOptionInfoDTO SpecificationAttributeOption { get; set; }
        public virtual ProductDescriptionTypeInfoDTO ProductDescriptionType { get; set; }
    }
}
