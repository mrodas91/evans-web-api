﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class CategoryPictureAddDTO
    {
        [Required]
        public string LocalPath { get; set; }

        [Required]
        public int CategoryId { get; set; }
    }
}
