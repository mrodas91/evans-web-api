﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EvansWebAPI.Models
{
    public class DefaultLanguageValuePutDTO
    {
        [Required]
        public string EnName { get; set; }
        [Required]
        public int EntityId { get; set; }
        [Required]
        public string KeyGroup { get; set; }
    }
}
