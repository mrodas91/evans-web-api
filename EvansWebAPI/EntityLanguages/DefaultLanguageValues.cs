﻿using System;
namespace EvansWebAPI.EntityLanguages
{
    public class DefaultLanguageValues
    {
        public string EnName { get; set; }

        public string EnShortDescription { get; set; }

        public string EnFullDescription { get; set; }

        public int? EntityId { get; set; }
    }
}
