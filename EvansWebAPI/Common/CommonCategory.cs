﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EvansWebAPI.Contexts;
using EvansWebAPI.Entities;
using EvansWebAPI.EntityLanguages;
using EvansWebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace EvansWebAPI.Common
{
    public class CommonCategory
    {
        private readonly ApplicationDbContext context;
        private readonly IConfiguration configuration;
        private readonly CommonMethods commonMethods;

        public CommonCategory(ApplicationDbContext context, IConfiguration configuration, CommonMethods commonMethods)
        {
            this.context = context;
            this.configuration = configuration;
            this.commonMethods = commonMethods;
        }

        public bool CategoryExist(int categoryId)
        {
            return context.Category.FirstOrDefault(x => x.Id == categoryId) != null;
        }

        public bool IsMegaFamilia(int categoryId)
        {
            return context.Category.Where(x => x.Id == categoryId).Select(s => s.ParentCategoryId).FirstOrDefault() == 0;
        }

        public bool IsFamilia(int categoryId)
        {
            return context.Category
                .Where(x => x.Id == context.Category
                    .Where(x => x.Id == categoryId)
                    .Select(s => s.ParentCategoryId).FirstOrDefault())
                .Select(s => s.ParentCategoryId).FirstOrDefault() == 0;
        }

        public int ValidateProductCategoryRelation(int categoryId)
        {
            if (!CategoryExist(categoryId))
                return 1;
            if (IsMegaFamilia(categoryId))
                return 2;
            if (IsFamilia(categoryId))
                return 3;

            return 0;
        }

        public async Task<List<CategoryTreeDTO>> GetAllByName(string name)
        {
            using (SqlConnection sql = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                using (SqlCommand command = new SqlCommand("GetArbolCategorias", sql))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    if (name is not null)
                        command.Parameters.Add(new SqlParameter("@pCategoria", name));
                    await sql.OpenAsync();

                    var response = new List<CategoryTreeDTO>();
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            CategoryTreeDTO tree = new CategoryTreeDTO();

                            tree.MegaFamiliaId = (int)reader["MegaFamiliaId"];
                            tree.MegaFamiliaNombre = reader["MegaFamiliaNombre"].ToString();
                            tree.MegaFamiliaParent = (int)reader["MegaFamiliaParent"];

                            if (reader["FamiliaId"] != DBNull.Value)
                            {
                                tree.FamiliaId = (int)reader["FamiliaId"];
                                tree.FamiliaNombre = reader["FamiliaNombre"].ToString();
                                tree.FamiliaParent = (int)reader["FamiliaParent"];
                            }

                            if (reader["CategoriaId"] != DBNull.Value)
                            {
                                tree.CategoriaId = (int)reader["CategoriaId"];
                                tree.CategoriaNombre = reader["CategoriaNombre"].ToString();
                                tree.CategoriaParent = (int)reader["CategoriaParent"];
                            }
                            response.Add(tree);
                        }
                    }
                    return response;
                }
            }
        }

        public bool IsCollection(int categoryId)
        {
            return context.Category.Where(x => x.Id == categoryId).Select(s => s.IsCollection).FirstOrDefault() == true;
        }

        public bool IsPromotion(int categoryId)
        {
            return context.Category.
                Where(x => x.Id == categoryId).Select(x => x.ParentCategoryId).First() == Convert.ToInt32(configuration["DefaultDbValues:PromotionCategoryId"]);
        }

        public async Task<bool> AddCategoryPicture(string path, Category category)
        {
            try
            {
                var imageFromFile = System.Drawing.Image.FromFile(path);
                var stream = new System.IO.MemoryStream();
                imageFromFile.Save(stream, imageFromFile.RawFormat);
                stream.Position = 0;
                var formFile = new FormFile(stream, 0, stream.Length, "streamFile", path.Split(@"\").Last())
                {
                    Headers = new HeaderDictionary(),
                    ContentType = string.Empty
                };

                Picture picture = commonMethods.InsertPicture(formFile);

                category.PictureId = picture.Id;
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
