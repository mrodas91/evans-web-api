﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EvansWebAPI.Contexts;
using EvansWebAPI.Entities;
using EvansWebAPI.EntityLanguages;
using EvansWebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Formats.Bmp;
using SixLabors.ImageSharp.Formats.Gif;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using static System.Net.Mime.MediaTypeNames;

namespace EvansWebAPI.Common
{
    public class CommonMethods
    {
        private readonly ApplicationDbContext context;
        private readonly IConfiguration configuration;

        public CommonMethods(ApplicationDbContext context, IConfiguration _configuration)
        {
            this.context = context;
            this.configuration = _configuration;
        }

        private LocalizedProperty AddEnglishValues(int _entityId, string _tableName, string _columnName, string _value)
        {
            return new LocalizedProperty {
                EntityId = _entityId,
                LocaleKeyGroup = _tableName,
                LocaleKey = _columnName,
                LocaleValue = _value,
                LanguageId = Convert.ToInt32(configuration["DefaultDbValues:EnglishLanguageId"])
            };
        }

        public async Task<bool> AddEnglishValues(DefaultLanguageValues englishValues, string group, int entityId)
        {
            try
            {
                //For Existing properties
                List<LocalizedProperty> propertiesToUpdate = await context.LocalizedProperty
                    .Where(x => x.LocaleKeyGroup == group && x.EntityId == entityId).ToListAsync();

                foreach(LocalizedProperty property in propertiesToUpdate)
                {
                    if (property.LocaleKey == "Name" && englishValues.EnName is not null)
                    {
                        property.LocaleValue = englishValues.EnName;

                        //Update URL Record
                        if (group == "Product" || group == "Category")
                            await UpdateUrlRecord(group, entityId, englishValues.EnName, Convert.ToInt32(configuration["DefaultDbValues:EnglishLanguageId"]));
                    }
                    else if (property.LocaleKey == "ShortDescription" && englishValues.EnShortDescription is not null)
                        property.LocaleValue = englishValues.EnShortDescription;
                    else if (property.LocaleKey == "Description" && englishValues.EnShortDescription is not null)
                        property.LocaleValue = englishValues.EnShortDescription;
                    else if (property.LocaleKey == "FullDescription" && englishValues.EnFullDescription is not null)
                        property.LocaleValue = englishValues.EnFullDescription;
                }
                await context.SaveChangesAsync();

                //For new properties
                List<LocalizedProperty> newProperties = new List<LocalizedProperty>();
                if (!propertiesToUpdate.Exists(x => x.LocaleKey == "Name") && englishValues.EnName is not null)
                    newProperties.Add(AddEnglishValues(entityId, group, "Name", englishValues.EnName));

                if(group == "Product" || group == "Category")
                    //Create URL Record
                    if (!propertiesToUpdate.Exists(x => x.LocaleKey == "Name") && englishValues.EnName is not null)
                        await CreateUrlRecord(group, entityId, englishValues.EnName, Convert.ToInt32(configuration["DefaultDbValues:EnglishLanguageId"]));

                switch (group)
                {
                    case "Product":
                        if (!propertiesToUpdate.Exists(x => x.LocaleKey == "ShortDescription") && englishValues.EnShortDescription is not null)
                            newProperties.Add(AddEnglishValues(entityId, group, "ShortDescription", englishValues.EnShortDescription));
                        if (!propertiesToUpdate.Exists(x => x.LocaleKey == "FullDescription") && englishValues.EnFullDescription is not null)
                            newProperties.Add(AddEnglishValues(entityId, group, "FullDescription", englishValues.EnFullDescription));
                        break;

                    case "Category":
                        if (!propertiesToUpdate.Exists(x => x.LocaleKey == "Description") && englishValues.EnShortDescription is not null)
                            newProperties.Add(AddEnglishValues(entityId, group, "Description", englishValues.EnShortDescription));
                        break;
                }

                if (newProperties.Count > 0)
                {
                    await context.LocalizedProperty.AddRangeAsync(newProperties);
                    await context.SaveChangesAsync();
                }
                
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public async Task<bool> AddEnglishValues(List<DefaultLanguageValues> englishValues, string group)
        {
            try
            {
                //For Existing properties
                List<LocalizedProperty> propertiesToUpdate = context.LocalizedProperty.ToList().
                    Where(x => englishValues.Any(y => x.EntityId == y.EntityId && x.LocaleKeyGroup == group)).ToList();

                foreach (LocalizedProperty property in propertiesToUpdate)
                {
                    List<DefaultLanguageValues> valuesList = englishValues.Where(y => y.EntityId == property.EntityId).ToList();

                    foreach(DefaultLanguageValues value in valuesList)
                    {
                        if (property.LocaleKey == "Name" && value.EnName is not null)
                        {
                            property.LocaleValue = value.EnName;

                            if (group == "Product" || group == "Category")
                                await UpdateUrlRecord(group, property.EntityId, value.EnName, Convert.ToInt32(configuration["DefaultDbValues:EnglishLanguageId"]));
                        }
                        else if (property.LocaleKey == "ShortDescription" && value.EnShortDescription is not null)
                            property.LocaleValue = value.EnShortDescription;
                        else if (property.LocaleKey == "Description" && value.EnShortDescription is not null)
                            property.LocaleValue = value.EnShortDescription;
                        else if (property.LocaleKey == "FullDescription" && value.EnFullDescription is not null)
                            property.LocaleValue = value.EnFullDescription;
                    }
                }
                await context.SaveChangesAsync();

                //For new properties
                List<LocalizedProperty> newProperties = new List<LocalizedProperty>();
                foreach (DefaultLanguageValues value in englishValues)
                {
                    List<LocalizedProperty> existingProperty = propertiesToUpdate.Where(x => x.EntityId == value.EntityId).ToList();

                    if (!existingProperty.Exists(x => x.LocaleKey == "Name") && value.EnName is not null)
                        newProperties.Add(AddEnglishValues((int)value.EntityId, group, "Name", value.EnName));

                    if (group == "Product" || group == "Category")
                        //Create URL Record
                        if (!propertiesToUpdate.Exists(x => x.LocaleKey == "Name") && value.EnName is not null)
                            await CreateUrlRecord(group, (int)value.EntityId, value.EnName, Convert.ToInt32(configuration["DefaultDbValues:EnglishLanguageId"]));

                    switch (group)
                    {
                        case "Product":
                            if (!existingProperty.Exists(x => x.LocaleKey == "ShortDescription") && value.EnShortDescription is not null)
                                newProperties.Add(AddEnglishValues((int)value.EntityId, group, "ShortDescription", value.EnShortDescription));
                            if (!existingProperty.Exists(x => x.LocaleKey == "FullDescription") && value.EnFullDescription is not null)
                                newProperties.Add(AddEnglishValues((int)value.EntityId, group, "FullDescription", value.EnFullDescription));
                            break;

                        case "Category":
                            if (!existingProperty.Exists(x => x.LocaleKey == "Description") && value.EnShortDescription is not null)
                                newProperties.Add(AddEnglishValues((int)value.EntityId, group, "Description", value.EnShortDescription));
                            break;
                    }                    
                }

                if (newProperties.Count > 0)
                {
                    await context.LocalizedProperty.AddRangeAsync(newProperties);
                    await context.SaveChangesAsync();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> DeleteEnglishValues(string group, int entityId)
        {
            try
            {
                List<LocalizedProperty> propertiesToDelete = await context.LocalizedProperty
                    .Where(x => x.LocaleKeyGroup == group && x.EntityId == entityId).ToListAsync();

                context.LocalizedProperty.RemoveRange(propertiesToDelete);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region UrlRecord Methods
        public async Task<bool> CreateUrlRecord(string _tableName, int _entityId, string _slugValue, int _languageId)
        {
            try
            {
                //Remove tildes
                byte[] bytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(_slugValue.Trim().ToLower());
                context.UrlRecord.Add(new UrlRecord
                {
                    EntityName = _tableName,
                    Slug = Regex.Replace(System.Text.Encoding.UTF8.GetString(bytes), @"\s+", "-"),
                    EntityId = _entityId,
                    IsActive = true,
                    LanguageId = _languageId
                });
                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> UpdateUrlRecord(string _tableName, int _entityId, string _slugValue, int _languageId, bool _IsActive = true)
        {
            try
            {
                UrlRecord recordToUpdate = await context.UrlRecord
                    .FirstOrDefaultAsync(x => x.EntityName == _tableName && x.EntityId == _entityId && x.LanguageId == _languageId);
                //Remove tildes
                byte[] bytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(_slugValue.ToLower());
                recordToUpdate.Slug = Regex.Replace(System.Text.Encoding.UTF8.GetString(bytes), @"\s+", "-");
                recordToUpdate.IsActive = _IsActive;
                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> DeleteUrlRecords(string _tableName, int _entityId)
        {
            try
            {
                List<UrlRecord> recordsToDelete = await context.UrlRecord
                    .Where(x => x.EntityName == _tableName && x.EntityId == _entityId).ToListAsync();

                context.UrlRecord.RemoveRange(recordsToDelete);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> EnableDisableUrlRecords(string _tableName, int _entityId)
        {
            try
            {
                List<UrlRecord> recordsToUpdate = await context.UrlRecord
                    .Where(x => x.EntityName == _tableName && x.EntityId == _entityId).ToListAsync();

                recordsToUpdate.ForEach(x => x.IsActive = !x.IsActive);

                context.UrlRecord.UpdateRange(recordsToUpdate);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        public bool ProductExits(int productId)
        {
            return context.Product.Where(x => x.Id == productId).Count() > 0;
        }


        //using (WebClient webClient = new WebClient())
        //{
        //    byte[] data = webClient.DownloadData(categoryDTO.PicturePath);
        //    using var stream = new MemoryStream(data);
        //    //using (var stream = System.IO.File.Create(categoryDTO.PicturePath))
        //    //{
        //    var formFile = new FormFile(stream, 0, stream.Length, "streamFile", categoryDTO.PicturePath.Split(@"\").Last())
        //    {
        //        Headers = new HeaderDictionary(),
        //        ContentType = string.Empty
        //    };
        //    //await formFile.CopyToAsync(stream);
        //    var picture = commonMethods.InsertPicture(formFile);
        //    //}
        //    category.PictureId = picture.Id;
        //    await context.SaveChangesAsync();
        //}

        //var imageFromFile = Image.FromFile(categoryDTO.PicturePath);
        //var stream = new System.IO.MemoryStream();
        //imageFromFile.Save(stream, imageFromFile.RawFormat);
        //stream.Position = 0;
        //var formFile = new FormFile(stream, 0, stream.Length, "streamFile", categoryDTO.PicturePath.Split(@"\").Last())
        //{
        //    Headers = new HeaderDictionary(),
        //    ContentType = string.Empty
        //};
        //var picture = commonMethods.InsertPicture(formFile);
        //category.PictureId = picture.Id;
        //await context.SaveChangesAsync();
        public async Task<bool> AddProductPicture(string path, Product product, int displayOrder = 0)
        {
            try
            {
                var imageFromFile = System.Drawing.Image.FromFile(path);
                var stream = new System.IO.MemoryStream();
                imageFromFile.Save(stream, imageFromFile.RawFormat);
                stream.Position = 0;
                var formFile = new FormFile(stream, 0, stream.Length, "streamFile", path.Split(@"\").Last())
                {
                    Headers = new HeaderDictionary(),
                    ContentType = string.Empty
                };
                Picture picture = InsertPicture(formFile);

                ProductPictureMapping pictureMapping = new ProductPictureMapping { PictureId = picture.Id, ProductId = product.Id, DisplayOrder = displayOrder };
                await context.ProductPictureMapping.AddAsync(pictureMapping);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Ensure that a string doesn't exceed maximum allowed length
        /// </summary>
        /// <param name="str">Input string</param>
        /// <param name="maxLength">Maximum length</param>
        /// <param name="postfix">A string to add to the end if the original string was shorten</param>
        /// <returns>Input string if its length is OK; otherwise, truncated input string</returns>
        public static string EnsureMaximumLength(string str, int maxLength, string postfix = null)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (str.Length <= maxLength)
                return str;

            var pLen = postfix?.Length ?? 0;

            var result = str.Substring(0, maxLength - pLen);
            if (!string.IsNullOrEmpty(postfix))
            {
                result += postfix;
            }

            return result;
        }

        /// <summary>
        /// Validates input picture dimensions
        /// </summary>
        /// <param name="pictureBinary">Picture binary</param>
        /// <param name="mimeType">MIME type</param>
        /// <returns>Picture binary or throws an exception</returns>
        public virtual byte[] ValidatePicture(byte[] pictureBinary, string mimeType)
        {
            using var image = SixLabors.ImageSharp.Image.Load<Rgba32>(pictureBinary, out var imageFormat);
            //resize the image in accordance with the maximum size
            if (Math.Max(image.Height, image.Width) > 1980) //_mediaSettings.MaximumImageSize
            {
                image.Mutate(imageProcess => imageProcess.Resize(new ResizeOptions
                {
                    Mode = ResizeMode.Max,
                    Size = new Size(1980) //_mediaSettings.MaximumImageSize
                }));
            }

            return EncodeImage(image, imageFormat);
        }

        /// <summary>
        /// Encode the image into a byte array in accordance with the specified image format
        /// </summary>
        /// <typeparam name="TPixel">Pixel data type</typeparam>
        /// <param name="image">Image data</param>
        /// <param name="imageFormat">Image format</param>
        /// <param name="quality">Quality index that will be used to encode the image</param>
        /// <returns>Image binary data</returns>
        protected virtual byte[] EncodeImage<TPixel>(Image<TPixel> image, IImageFormat imageFormat, int? quality = null)
            where TPixel : unmanaged, IPixel<TPixel>
        {
            using var stream = new MemoryStream();
            //var imageEncoder = Default.ImageFormatsManager.FindEncoder(imageFormat);
            ImageFormatManager ifm = Configuration.Default.ImageFormatsManager; //new ImageFormatManager();
            var imageEncoder = ifm.FindEncoder(imageFormat);
            switch (imageEncoder)
            {
                case JpegEncoder jpegEncoder:
                    jpegEncoder.Subsample = JpegSubsample.Ratio444;
                    jpegEncoder.Quality = quality ?? 80; //_mediaSettings.DefaultImageQuality
                    jpegEncoder.Encode(image, stream);
                    break;

                case PngEncoder pngEncoder:
                    pngEncoder.ColorType = PngColorType.RgbWithAlpha;
                    pngEncoder.Encode(image, stream);
                    break;

                case BmpEncoder bmpEncoder:
                    bmpEncoder.BitsPerPixel = BmpBitsPerPixel.Pixel32;
                    bmpEncoder.Encode(image, stream);
                    break;

                case GifEncoder gifEncoder:
                    gifEncoder.Encode(image, stream);
                    break;

                default:
                    imageEncoder.Encode(image, stream);
                    break;
            }

            return stream.ToArray();
        }

        /// <summary>
        /// Gets the download binary array
        /// </summary>
        /// <param name="file">File</param>
        /// <returns>Download binary array</returns>
        public virtual byte[] GetDownloadBits(IFormFile file)
        {
            using var fileStream = file.OpenReadStream();
            using var ms = new MemoryStream();
            fileStream.CopyTo(ms);
            var fileBytes = ms.ToArray();
            return fileBytes;
        }

        /// <summary>
        /// Updates the picture binary data
        /// </summary>
        /// <param name="picture">The picture object</param>
        /// <param name="binaryData">The picture binary data</param>
        /// <returns>Picture binary</returns>
        protected virtual PictureBinary UpdatePictureBinary(Picture picture, byte[] binaryData)
        {
            if (picture == null)
                throw new ArgumentNullException(nameof(picture));

            //var pictureBinary = GetPictureBinaryByPictureId(picture.Id);
            var pictureBinary = context.PictureBinary.FirstOrDefault(pb => pb.PictureId == picture.Id);

            var isNew = pictureBinary == null;

            if (isNew)
                pictureBinary = new PictureBinary
                {
                    PictureId = picture.Id
                };

            pictureBinary.BinaryData = binaryData;

            if (isNew)
            {
                //_pictureBinaryRepository.Insert(pictureBinary);
                context.PictureBinary.Add(pictureBinary);

                //event notification
                //_eventPublisher.EntityInserted(pictureBinary);
            }
            else
            {
                //_pictureBinaryRepository.Update(pictureBinary);
                context.PictureBinary.Update(pictureBinary);

                //event notification
                //_eventPublisher.EntityUpdated(pictureBinary);
            }
            context.SaveChanges();
            return pictureBinary;
        }

        /// <summary>
        /// Inserts a picture
        /// </summary>
        /// <param name="pictureBinary">The picture binary</param>
        /// <param name="mimeType">The picture MIME type</param>
        /// <param name="seoFilename">The SEO filename</param>
        /// <param name="altAttribute">"alt" attribute for "img" HTML element</param>
        /// <param name="titleAttribute">"title" attribute for "img" HTML element</param>
        /// <param name="isNew">A value indicating whether the picture is new</param>
        /// <param name="validateBinary">A value indicating whether to validated provided picture binary</param>
        /// <returns>Picture</returns>
        public virtual Picture InsertPicture(byte[] pictureBinary, string mimeType, string seoFilename,
            string altAttribute = null, string titleAttribute = null,
            bool isNew = true, bool validateBinary = true)
        {
            mimeType ??= string.Empty;
            mimeType = EnsureMaximumLength(mimeType, 20);

            seoFilename = EnsureMaximumLength(seoFilename, 100);

            if (validateBinary)
                pictureBinary = ValidatePicture(pictureBinary, mimeType);

            var picture = new Picture
            {
                MimeType = mimeType,
                SeoFilename = seoFilename,
                AltAttribute = altAttribute,
                TitleAttribute = titleAttribute,
                IsNew = isNew
            };

            context.Picture.Add(picture);
            context.SaveChanges();
            //_pictureRepository.Insert(picture);

            //UpdatePictureBinary(picture, StoreInDb ? pictureBinary : Array.Empty<byte>());
            UpdatePictureBinary(picture, pictureBinary);

            //if (!StoreInDb)
                //SavePictureInFile(picture.Id, pictureBinary, mimeType);

            //event notification
            //_eventPublisher.EntityInserted(picture);

            return picture;
        }

        /// <summary>
        /// Inserts a picture
        /// </summary>
        /// <param name="formFile">Form file</param>
        /// <param name="defaultFileName">File name which will be use if IFormFile.FileName not present</param>
        /// <param name="virtualPath">Virtual path</param>
        /// <returns>Picture</returns>
        public virtual Picture InsertPicture(IFormFile formFile, string defaultFileName = "", string virtualPath = "")
        {
            var imgExt = new List<string>
            {
                ".bmp",
                ".gif",
                ".jpeg",
                ".jpg",
                ".jpe",
                ".jfif",
                ".pjpeg",
                ".pjp",
                ".png",
                ".tiff",
                ".tif"
            } as IReadOnlyCollection<string>;

            var fileName = formFile.FileName;
            if (string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(defaultFileName))
                fileName = defaultFileName;

            //remove path (passed in IE)
            //fileName = _fileProvider.GetFileName(fileName)

            var contentType = formFile.ContentType;

            //var fileExtension = _fileProvider.GetFileExtension(fileName);
            var fileExtension = Path.GetExtension(fileName);
            if (!string.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();

            if (imgExt.All(ext => !ext.Equals(fileExtension, StringComparison.CurrentCultureIgnoreCase)))
                return null;

            //contentType is not always available 
            //that's why we manually update it here
            //http://www.sfsu.edu/training/mimetype.htm
            if (string.IsNullOrEmpty(contentType))
            {
                switch (fileExtension)
                {
                    case ".bmp":
                        contentType = MimeTypes.ImageBmp;
                        break;
                    case ".gif":
                        contentType = MimeTypes.ImageGif;
                        break;
                    case ".jpeg":
                    case ".jpg":
                    case ".jpe":
                    case ".jfif":
                    case ".pjpeg":
                    case ".pjp":
                        contentType = MimeTypes.ImageJpeg;
                        break;
                    case ".png":
                        contentType = MimeTypes.ImagePng;
                        break;
                    case ".tiff":
                    case ".tif":
                        contentType = MimeTypes.ImageTiff;
                        break;
                    default:
                        break;
                }
            }

            var picture = InsertPicture(GetDownloadBits(formFile), contentType, Path.GetFileNameWithoutExtension(fileName));

            if (string.IsNullOrEmpty(virtualPath))
                return picture;

            //picture.VirtualPath = _fileProvider.GetVirtualPath(virtualPath);
            //UpdatePicture(picture);

            return picture;
        }



    }
}
