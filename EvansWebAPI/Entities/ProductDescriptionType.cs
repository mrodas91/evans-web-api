﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class ProductDescriptionType
    {
        public ProductDescriptionType()
        {
            ProductDescriptionValue = new HashSet<ProductDescriptionValue>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProductDescriptionValue> ProductDescriptionValue { get; set; }
        public virtual ICollection<ProductSpecificationAttributeMapping> ProductSpecificationAttributeMapping { get; set; }
    }
}
