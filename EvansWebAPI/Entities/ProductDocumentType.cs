﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class ProductDocumentType
    {
        public ProductDocumentType()
        {
            ProductDocument = new HashSet<ProductDocument>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProductDocument> ProductDocument { get; set; }
    }
}
