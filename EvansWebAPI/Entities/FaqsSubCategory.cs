﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class FaqsSubCategory
    {
        public FaqsSubCategory()
        {
            Faqs = new HashSet<Faqs>();
        }

        public int Id { get; set; }
        public int IdFaqsCategory { get; set; }
        public string Name { get; set; }
        public int SubCategoryOrder { get; set; }
        public bool Enable { get; set; }

        public virtual FaqsCategory IdFaqsCategoryNavigation { get; set; }
        public virtual ICollection<Faqs> Faqs { get; set; }
    }
}
