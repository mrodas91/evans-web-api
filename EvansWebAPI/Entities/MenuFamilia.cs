﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class MenuFamilia
    {
        public long IdFamilia { get; set; }
        public long IdLenguaje { get; set; }
        public string Familia { get; set; }
        public string Slogan { get; set; }
        public string ImagenFamilia { get; set; }
        public string TextoAlternativoSlogan { get; set; }
        public string ColorSlogan { get; set; }
        public string ColorRuta { get; set; }
        public string DescripcionCaracteristica { get; set; }
        public string ImagenCaracteristica { get; set; }
        public string TextoAlternativoCaracteristica { get; set; }
        public string Descripcion { get; set; }
        public string ImagenBanner { get; set; }
        public string TextoAlternativoFamilia { get; set; }
        public short? IdTipoOrden { get; set; }
        public string TieneSelectorRapido { get; set; }
        public string ShortName { get; set; }
        public int? Nivel { get; set; }
        public long? IdPadre { get; set; }
        public int? IdCatMagento { get; set; }
    }
}
