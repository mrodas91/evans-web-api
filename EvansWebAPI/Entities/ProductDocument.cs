﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class ProductDocument
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string DocPath { get; set; }
        public int ProductDocumentTypeId { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual Product Product { get; set; }
        public virtual ProductDocumentType ProductDocumentType { get; set; }
    }
}
