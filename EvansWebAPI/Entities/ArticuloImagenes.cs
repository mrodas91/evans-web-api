﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class ArticuloImagenes
    {
        public long? IdArticulo { get; set; }
        public string CodigoArticulo { get; set; }
        public string RutaOrigen { get; set; }
        public string RutaDestinoWeb { get; set; }
        public string Foto1 { get; set; }
        public string Foto2 { get; set; }
        public string Foto3 { get; set; }
        public string Foto4 { get; set; }
        public string Foto5 { get; set; }
        public string Foto5EsGrafica { get; set; }
        public string Foto1S { get; set; }
        public string Foto2S { get; set; }
        public string Foto3S { get; set; }
        public string Foto4S { get; set; }
        public string Foto5S { get; set; }
        public string Foto1L { get; set; }
        public string Foto2L { get; set; }
        public string Foto3L { get; set; }
        public string Foto4L { get; set; }
        public string Foto5L { get; set; }
        public string FileMagentoImg1 { get; set; }
        public string FileMagentoImg2 { get; set; }
        public string FileMagentoImg3 { get; set; }
        public string FileMagentoImg4 { get; set; }
        public string FileMagentoImg5 { get; set; }
        public string FileMagentoImg6 { get; set; }
    }
}
