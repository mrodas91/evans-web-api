﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class PollAnswer
    {
        public PollAnswer()
        {
            PollVotingRecord = new HashSet<PollVotingRecord>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int PollId { get; set; }
        public int NumberOfVotes { get; set; }
        public int DisplayOrder { get; set; }

        public virtual Poll Poll { get; set; }
        public virtual ICollection<PollVotingRecord> PollVotingRecord { get; set; }
    }
}
