﻿using System;
using System.Collections.Generic;
using EvansWebAPI.EntityLanguages;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class ProductSpecificationAttributeMapping
    {
        public int Id { get; set; }
        public string CustomValue { get; set; }
        public int ProductId { get; set; }
        public int SpecificationAttributeOptionId { get; set; }
        public int AttributeTypeId { get; set; }
        public bool AllowFiltering { get; set; }
        public bool ShowOnProductPage { get; set; }
        public int DisplayOrder { get; set; }
        public int ProductDescriptionTypeId { get; set; }

        public virtual Product Product { get; set; }
        public virtual SpecificationAttributeOption SpecificationAttributeOption { get; set; }
        public virtual ProductDescriptionType ProductDescriptionType { get; set; }
    }
}
