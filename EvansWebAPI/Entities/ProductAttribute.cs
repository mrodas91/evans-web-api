﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class ProductAttribute
    {
        public ProductAttribute()
        {
            PredefinedProductAttributeValue = new HashSet<PredefinedProductAttributeValue>();
            ProductProductAttributeMapping = new HashSet<ProductProductAttributeMapping>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<PredefinedProductAttributeValue> PredefinedProductAttributeValue { get; set; }
        public virtual ICollection<ProductProductAttributeMapping> ProductProductAttributeMapping { get; set; }
    }
}
