﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class ArticuloReglas
    {
        public long? IdRegla { get; set; }
        public long? IdLenguaje { get; set; }
        public string Descripcion { get; set; }
        public string Campo { get; set; }
        public string UnidadMedida { get; set; }
        public short? OrdenDespliegue { get; set; }
        public string DatoFijo { get; set; }
        public string EsB2c { get; set; }
        public string EsSpec { get; set; }
        public int? IdMagento { get; set; }
    }
}
