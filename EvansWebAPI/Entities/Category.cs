﻿using System;
using System.Collections.Generic;
using EvansWebAPI.EntityLanguages;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class Category
    {
        public Category()
        {
            DiscountAppliedToCategories = new HashSet<DiscountAppliedToCategories>();
            ProductCategoryMapping = new HashSet<ProductCategoryMapping>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaTitle { get; set; }
        public string PriceRanges { get; set; }
        public string PageSizeOptions { get; set; }
        public string Description { get; set; }
        public int CategoryTemplateId { get; set; }
        public string MetaDescription { get; set; }
        public int ParentCategoryId { get; set; }
        public int PictureId { get; set; }
        public int PageSize { get; set; }
        public bool AllowCustomersToSelectPageSize { get; set; }
        public bool ShowOnHomepage { get; set; }
        public bool IncludeInTopMenu { get; set; }
        public bool SubjectToAcl { get; set; }
        public bool LimitedToStores { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public int DisplayOrder { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
        public bool IsCollection { get; set; }
        public string BannerURL { get; set; }
        public bool IsVideo { get; set; }

        public virtual ICollection<DiscountAppliedToCategories> DiscountAppliedToCategories { get; set; }
        public virtual ICollection<ProductCategoryMapping> ProductCategoryMapping { get; set; }
        public virtual DefaultLanguageValues CategoryLanguageValues { get; set; }

        public static implicit operator List<object>(Category v)
        {
            throw new NotImplementedException();
        }
    }
}
