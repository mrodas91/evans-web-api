﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class Politicas
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public int ShowOrder { get; set; }
        public bool Enable { get; set; }
    }
}
