﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class ArticuloGeneral
    {
        public long IdArticulo { get; set; }
        public long? IdFamilia { get; set; }
        public string Estatus { get; set; }
        public string CodigoArticulo { get; set; }
        public string Descripcion { get; set; }
        public string ExtraDescripcion { get; set; }
        public string Catalogo { get; set; }
        public string CodigoBarras { get; set; }
        public string Tipo { get; set; }
        public string Clase { get; set; }
        public string CodigoIva { get; set; }
        public decimal? PrecioLista { get; set; }
        public decimal? PrecioInternet { get; set; }
        public decimal? PrecioPromo { get; set; }
        public DateTime? VigenciaPromoIni { get; set; }
        public DateTime? VigenciaPromoFin { get; set; }
        public string Moneda { get; set; }
        public string Asignacion { get; set; }
        public string CodigoAbc { get; set; }
        public DateTime? FechaAlta { get; set; }
        public int? PeriodoNuevo { get; set; }
        public string Comprador { get; set; }
        public int? ProveedorPrim { get; set; }
        public int? ProveedorSec { get; set; }
        public string PaisOrigen { get; set; }
        public decimal? CostoStd { get; set; }
        public decimal? CostoAct { get; set; }
        public int? TiempoEntrega { get; set; }
        public decimal? StockMax { get; set; }
        public decimal? StockMin { get; set; }
        public decimal? PuntoOrden { get; set; }
        public string RutaManualUsr { get; set; }
        public string RutaManualUsrWeb { get; set; }
        public string AlmacenDefault { get; set; }
        public string LocalizacionDefault { get; set; }
        public string EsB2c { get; set; }
        public long? IdRegla { get; set; }
        public string EnUso { get; set; }
        public decimal? PesoEmbarque { get; set; }
        public decimal? CostoEmbarque { get; set; }
        public string UsrAlta { get; set; }
        public string Retenido { get; set; }
        public int? IdMagento { get; set; }
        public byte? ActualizarPrecio { get; set; }
    }
}
