﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class Log
    {
        public int Id { get; set; }
        public string ShortMessage { get; set; }
        public string IpAddress { get; set; }
        public int? CustomerId { get; set; }
        public int LogLevelId { get; set; }
        public string FullMessage { get; set; }
        public string PageUrl { get; set; }
        public string ReferrerUrl { get; set; }
        public DateTime CreatedOnUtc { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
