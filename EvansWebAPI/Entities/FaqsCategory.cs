﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class FaqsCategory
    {
        public FaqsCategory()
        {
            Faqs = new HashSet<Faqs>();
            FaqsSubCategory = new HashSet<FaqsSubCategory>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryOrder { get; set; }
        public bool Enable { get; set; }

        public virtual ICollection<Faqs> Faqs { get; set; }
        public virtual ICollection<FaqsSubCategory> FaqsSubCategory { get; set; }
    }
}
