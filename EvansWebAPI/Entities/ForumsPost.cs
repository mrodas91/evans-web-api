﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class ForumsPost
    {
        public ForumsPost()
        {
            ForumsPostVote = new HashSet<ForumsPostVote>();
        }

        public int Id { get; set; }
        public string Text { get; set; }
        public string Ipaddress { get; set; }
        public int CustomerId { get; set; }
        public int TopicId { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
        public int VoteCount { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ForumsTopic Topic { get; set; }
        public virtual ICollection<ForumsPostVote> ForumsPostVote { get; set; }
    }
}
