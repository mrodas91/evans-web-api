﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class Faqs
    {
        public int Id { get; set; }
        public int IdFaqsCategory { get; set; }
        public int? IdFaqsSubCategory { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int QuestionOrder { get; set; }
        public bool Enable { get; set; }

        public virtual FaqsCategory IdFaqsCategoryNavigation { get; set; }
        public virtual FaqsSubCategory IdFaqsSubCategoryNavigation { get; set; }
    }
}
