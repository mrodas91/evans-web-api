﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class MonedasTipoCambio
    {
        public string MonedaDe { get; set; }
        public string MonedaA { get; set; }
        public string Tipo { get; set; }
        public DateTime? FechaCambio { get; set; }
        public decimal? FactorTipoCambio { get; set; }
        public string UsuarioAlta { get; set; }
        public DateTime? FechaAlta { get; set; }
        public long Serialcolumn { get; set; }
    }
}
