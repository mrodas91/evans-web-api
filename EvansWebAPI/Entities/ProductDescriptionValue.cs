﻿using System;
using System.Collections.Generic;
using EvansWebAPI.EntityLanguages;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class ProductDescriptionValue
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int ProductDescriptionTypeId { get; set; }
        public int ProductDescriptionNameId { get; set; }
        public string Value { get; set; }
        public int DisplayOrder { get; set; }
        public DateTime UpdateDate { get; set; }

        public virtual Product Product { get; set; }
        public virtual ProductDescriptionName ProductDescriptionName { get; set; }
        public virtual ProductDescriptionType ProductDescriptionType { get; set; }
        public virtual ICollection<DefaultLanguageValues> DefaultLanguageValues { get; set; }
    }
}
