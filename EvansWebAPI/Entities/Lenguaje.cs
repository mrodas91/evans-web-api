﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EvansWebAPI.Entities
{
    public partial class Lenguaje
    {
        public long IdLenguaje { get; set; }
        public string Lenguaje1 { get; set; }
        public string ImagenLenguaje { get; set; }
        public string TextoAlternativoLenguaje { get; set; }
        public string StrLenguaje { get; set; }
    }
}
